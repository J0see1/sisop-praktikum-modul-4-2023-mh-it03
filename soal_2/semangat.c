#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define MAX_PATH_LENGTH 1000
#define MAX_PASSWORD_LENGTH 1024
#define LOG_FILE "logs-fuse.log"

static char password[MAX_PASSWORD_LENGTH];
static const char *dirpath = "/home/josee/sisop-praktikum-modul-4-2023-mh-it03/soal_2/home";

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1) return -errno;

    return 0;
}

void write_log(const char *status, const char *tag, const char *info) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    FILE *log_file = fopen(LOG_FILE, "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
                status, tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, tag, info);
        fclose(log_file);
    }
}

void generateHTML(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");

    if (csvFile == NULL) {
        return;
    }

    char line[512];  // Assuming a maximum line length of 512 characters

    // Read the header line (file,title,body)
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unable to read the header line");
        return; // Unable to read the header line
    }

    // Check if the header line is as expected
    if (strcmp(line, "file,title,body\n") != 0) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unexpected header line");
        return; // Unexpected header line
    }

    // Get the directory of the CSV file
    char csvDir[512];
    strncpy(csvDir, csvPath, strrchr(csvPath, '/') - csvPath + 1);
    csvDir[strrchr(csvPath, '/') - csvPath + 1] = '\0';

    // Continue reading the rest of the CSV file
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char fileName[256], title[256], body[256];

        sscanf(line, "%[^,],%[^,],%[^\n]", fileName, title, body);

        char htmlFileName[512];
        sprintf(htmlFileName, "%s/website/%s.html", dirpath, fileName);

        FILE *htmlFile = fopen(htmlFileName, "w");

        if (htmlFile == NULL) {
            perror("Error creating HTML file");
            write_log("FAILED", "generateHTML", "Error creating HTML file");
            fclose(csvFile);
            return;
        }

        // Write the HTML content to the file
        fprintf(htmlFile, "<!DOCTYPE html>\n");
        fprintf(htmlFile, "<html lang=\"en\">\n");
        fprintf(htmlFile, "<head>\n");
        fprintf(htmlFile, "\t<meta charset=\"UTF-8\" />\n");
        fprintf(htmlFile, "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
        fprintf(htmlFile, "\t<title>%s</title>\n", title);
        fprintf(htmlFile, "</head>\n");
        fprintf(htmlFile, "<body>\n");
        fprintf(htmlFile, "\t%s\n", body);
        fprintf(htmlFile, "</body>\n");
        fprintf(htmlFile, "</html>\n");

        fclose(htmlFile);
        write_log("SUCCESS", "generateHTML", htmlFileName);
    }

    fclose(csvFile);
}

void classifyAndMoveFile(const char *fileName) {
    const char *extensions[] = {".pdf", ".docx", ".jpg", ".png", ".ico", ".js", ".html", ".json", ".c", ".sh", ".txt", ".ipynb", ".csv"};
    const char *folders[] = {"documents", "documents", "images", "images", "images", "website", "website", "website", "sisop", "sisop", "text", "aI", "aI"};
    const char *extension = strrchr(fileName, '.');
    
    if (extension != NULL) {
        for (int i = 0; i < sizeof(extensions) / sizeof(extensions[0]); i++) {
            if (strcmp(extension, extensions[i]) == 0) {
                if (strcmp(extension, ".csv") == 0) {
                    // Assuming CSV files are in aI/
                    char csvPath[512];
                    sprintf(csvPath, "%s/aI/%s", dirpath, basename(fileName));
                    generateHTML(csvPath);

                    // Move the CSV file to the aI/ directory after processing
                    char destPath[512];
                    sprintf(destPath, "%s/aI/%s", dirpath, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                } else {
                    char destFolder[512];
                    sprintf(destFolder, "%s/%s", dirpath, folders[i]);
                    char destPath[512];
                    sprintf(destPath, "%s/%s", destFolder, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                }
                return;
            }
        }
    }
}

void processFilesInDirectory(const char* directoryPath) {
    DIR *dir;
    struct dirent *entry;
    struct stat statBuf;
    char filePath[512];

    dir = opendir(directoryPath);

    if (!dir) {
        perror("Directory tidak dapat dibuka");
        write_log("FAILED", "processFilesInDirectory", "Directory tidak dapat dibuka");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir))) {
        sprintf(filePath, "%s/%s", directoryPath, entry->d_name);

        if (!stat(filePath, &statBuf) && S_ISREG(statBuf.st_mode)) {
            classifyAndMoveFile(filePath);
            write_log("SUCCESS", "processFilesInDirectory", filePath);
        }
    }

    closedir(dir);
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];

    if(strcmp(path,"/") == 0) {
        path=dirpath;
        snprintf(fpath, sizeof(fpath), "%s", path);
    } else snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        write_log("FAILED", "readdir", strerror(errno));
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;

        if (de->d_type == DT_REG) {
            char file_path[MAX_PATH_LENGTH];
            snprintf(file_path, sizeof(file_path), "%s/%s", fpath, de->d_name);
            classifyAndMoveFile(file_path);
            write_log("SUCCESS", "readdir", file_path);
        }
    }

    closedir(dp);
    return 0;
}

static bool is_correct_password(const char *user_password) {
    read_password();
    write_log("SUCCESS", "is_correct_password", "Password read");

    return strcmp(user_password, password) == 0;
}

void read_password() {
    FILE *password_file = fopen("home/password.bin", "rb");

    if (password_file == NULL) {
        perror("Error opening password file");
        write_log("FAILED", "read_password", "Error opening password file");
        exit(EXIT_FAILURE);
    }

    fread(password, sizeof(char), MAX_PASSWORD_LENGTH, password_file);

    fclose(password_file);
    write_log("SUCCESS", "read_password", "Password read");
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        write_log("FAILED", "read", strerror(errno));
        return -errno;
    }

    if (strstr(fpath, "text/") != NULL) {
        bool correct_password = false;

        while (!correct_password) {
            char user_password[MAX_PASSWORD_LENGTH];
            printf("Enter password ");
            scanf("%s", user_password);

            correct_password = is_correct_password(user_password);

            if (!correct_password) {
                printf("Incorrect password.\n");
            }
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        write_log("FAILED", "read", strerror(errno));
        res = -errno;
    }

    close(fd);

    write_log("SUCCESS", "read", fpath);

    return res;
}

static int xmp_setfattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);
    if (res == -1) {
        write_log("FAILED", "setfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "setfattr", fpath);

    return 0;
}

static int xmp_getfattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lgetxattr(fpath, name, value, size);
    if (res == -1) {
        write_log("FAILED", "getfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "getfattr", fpath);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags);
    if (res == -1) {
        write_log("FAILED", "open", strerror(errno));
        return -errno;
    }

    fi->fh = res;

    write_log("SUCCESS", "open", fpath);

    return 0;
}

static int xmp_unlink(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the file or folder has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "unlink", "Deleting files/folders with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = unlink(fpath);
    if (res == -1) {
        write_log("FAILED", "unlink", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "unlink", fpath);

    return 0;
}


static int xmp_rmdir(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the directory has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "rmdir", "Deleting directories with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = rmdir(fpath);
    if (res == -1) {
        write_log("FAILED", "rmdir", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "rmdir", fpath);

    return 0;
}


static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int fd = open(fpath, fi->flags | O_WRONLY);
    if (fd == -1) {
        write_log("FAILED", "write", strerror(errno));
        return -errno;
    }

    // Move the file pointer to the specified offset
    if (lseek(fd, offset, SEEK_SET) == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    // Write data to the file
    ssize_t res = write(fd, buf, size);
    if (res == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    close(fd);

    // Log the successful write operation
    write_log("SUCCESS", "write", fpath);

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .setxattr = xmp_setfattr,
    .getxattr = xmp_getfattr,
    .open = xmp_open,
    .unlink = xmp_unlink, 
    .rmdir = xmp_rmdir, 
    .write = xmp_write,
};


int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(LOG_FILE, "w");
    if (log_file != NULL) {
        fclose(log_file);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

//  gcc -Wall -D_FILE_OFFSET_BITS=64 `pkg-config fuse --cflags` semangat.c -o semangat `pkg-config fuse --libs` -lfuse

// setfattr -n user.name -v "bangudahbang" Belajar-Python.pdf
// getfattr -n user.name Belajar-Python.pdf