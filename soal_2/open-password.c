#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include <string.h>

// gcc open-password.c -o open-password -lcrypto
// wget -q -O files.zip https://drive.google.com/u/0/uc?id=1MFQ6ds9lpbo9I6-QlXjyFPYyMEZpB9Tq&export=download

char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}

char *readFile(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("File tidak ditemukan");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *content = (char *)malloc(file_size + 1);
    if (content == NULL) {
        fclose(file);
        perror("Gagal mengalokasi memori");
        exit(1);
    }

    fread(content, 1, file_size, file);
    fclose(file);

    content[file_size] = '\0';

    return content;
}

int main() {
    system("unzip files.zip");
    char *password = readFile("zip-pass.txt");
    printf("Password dari zip-pass.txt: %s\n", password);

    char *decoded_password = decryptBase64(password);
    printf("Password setelah dekripsi base64: %s\n", decoded_password);

    char unzip_command[100];
    sprintf(unzip_command, "unzip -P %s home.zip -d home", decoded_password);
    system(unzip_command);


    free(password);
    free(decoded_password);

    return 0;
}
