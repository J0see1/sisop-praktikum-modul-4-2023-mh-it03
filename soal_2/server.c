#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

// Fungsi untuk memproses pesan dari client
void process_message(int new_socket, char *message) {
    FILE *file;
    char buffer[4096] = {0};
    char reply_message[4096] = {0};

    if (strncmp(message, "showAll", 7) == 0) {
        // Open the file webtoon.csv
        file = fopen("home/aI/webtoon.csv", "r");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Send webtoons to the client in chunks
        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, sizeof(buffer) - 1, file)) > 0) {
            buffer[bytesRead] = '\0';  // Null-terminate the buffer
            send(new_socket, buffer, bytesRead, 0);
            memset(buffer, 0, sizeof(buffer));
        }

        fclose(file);
} else if (strncmp(message, "showGenre:", 10) == 0) {
    // Extract the genre from the message
    char *genre = strtok(message + 11, " ");
    genre[strlen(genre) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified genre to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified genre
        if (strstr(buffer, genre) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "showDate:", 9) == 0) {
    // Extract the date from the message
    char *date = strtok(message + 10, " ");
    date[strlen(date) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified date to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified date
        if (strstr(buffer, date) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "add:", 4) == 0) {
        // Extract the new webtoon information from the message
        char *newWebtoon = strtok(message + 4, "\n");

        file = fopen("home/aI/webtoon.csv", "a");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Append the new webtoon to the file
        fprintf(file, "%s\n", newWebtoon);

        fclose(file);

        // Send a confirmation message to the client
        strcpy(reply_message, "Webtoon added successfully");
        send(new_socket, reply_message, strlen(reply_message), 0);

} else if (strncmp(message, "delete:", 7) == 0) {
    // Extract the content to be deleted from the message
    char* contentToDelete = message + 7;

    // Open the file webtoon.csv in read mode
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Open a temporary file in write mode
    FILE* tempFile = fopen("home/aI/temp_webtoon.csv", "w");
    if (!tempFile) {
        perror("Temporary file error");
        strcpy(reply_message, "Error creating temporary file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        fclose(file);
        return;
    }

    char line[1024];
    int contentDeleted = 0;

    // Read the file line by line
    while (fgets(line, sizeof(line), file)) {
        // Check if the line contains the content to be deleted
        if (strstr(line, contentToDelete) == NULL) {
            // If not, write the line to the temporary file
            fputs(line, tempFile);
        } else {
            // If yes, set a flag to indicate content deletion
            contentDeleted = 1;
        }
    }

    // Close both files
    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove("home/aI/webtoon.csv");
    rename("home/aI/temp_webtoon.csv", "home/aI/webtoon.csv");

    if (contentDeleted) {
        strcpy(reply_message, "Content deleted successfully");
    } else {
        strcpy(reply_message, "Content not found for deletion");
    }

    send(new_socket, reply_message, strlen(reply_message), 0);
} else {
        strcpy(reply_message, "Invalid request");
        send(new_socket, reply_message, strlen(reply_message), 0);
    }
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[4096] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket option to allow address reuse
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

while (1) {
    valread = read(new_socket, buffer, 1024);
    if (valread <= 0) {
        break;
    }

    // Remove the newline character if present
    size_t len = strlen(buffer);
    if (len > 0 && buffer[len - 1] == '\n') {
        buffer[len - 1] = '\0';
    }

    printf("Client: %s\n", buffer);

    process_message(new_socket, buffer);

    memset(buffer, 0, sizeof(buffer));
}

    close(new_socket);

    return 0;
}
