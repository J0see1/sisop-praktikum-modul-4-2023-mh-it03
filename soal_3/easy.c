#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>

static const char *fs_path = "/tmp/modular"; //menentukan path dari file system yang akan diatur
static const char *log_path = "/home/dave/fs_module.log"; //menentukan path untuk file log

//fungsi untuk mencatat sistem call ke dalam file log
void log_system_call(char *level, char *command, char *description) {
    time_t t; //variabel untuk menyimpan satuan waktu
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20]; //membuat format waktu 
    strftime(timestamp, 20, "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a"); //membuka file log untuk menambahkan data
    if (log_file) {
        fprintf(log_file, "[%s]::%s::%s::%s\n", level, timestamp, command, description);
        fclose(log_file);
    }
}

//fungsi untuk memodularisasi file dengan ukuran 1024 bytes
//membaca file dan membuatnya menjadi bagian - bagian kecil
void modularize_file(char *file_path) {
    FILE *original_file = fopen(file_path, "rb");
    if (original_file) {
        fseek(original_file, 0L, SEEK_END);
        long file_size = ftell(original_file);
        rewind(original_file);

        if (file_size > 1024) {
            char buffer[1024];
            int index = 0;

            while (1) {
                size_t read_size = fread(buffer, 1, sizeof(buffer), original_file);

                if (read_size > 0) {
                    char new_file_path[256];
                    snprintf(new_file_path, sizeof(new_file_path), "%s.%03d", file_path, index);
                    FILE *new_file = fopen(new_file_path, "wb");

                    if (new_file) {
                        fwrite(buffer, 1, read_size, new_file);
                        fclose(new_file);
                    }

                    index++;
                } else {
                    break;
                }
            }

            fclose(original_file);
            remove(file_path);
        }
    }
}

//fungsi untuk memodularisasi seluruh isi dari directory bahkan subdirectory
//melakukan rekursi ke dalam sub directory
void modularize_directory(const char *directory) {
    DIR *dir = opendir(directory);
    struct dirent *entry;

    if (dir) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) {
                char file_path[256];
                snprintf(file_path, sizeof(file_path), "%s/%s", directory, entry->d_name);
                modularize_file(file_path);
            } else if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                char subdirectory[256];
                snprintf(subdirectory, sizeof(subdirectory), "%s/%s", directory, entry->d_name);
                modularize_directory(subdirectory);
            }
        }

        closedir(dir);
    }
}

static int fs_getattr(const char *path, struct stat *stbuf) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return res;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dir;
    struct dirent *entry;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    dir = opendir(fpath);
    if (dir == NULL)
        return -errno;

    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0))
            break;
    }

    closedir(dir);
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;

    if (strstr(path, "module_") != NULL) {
        log_system_call("REPORT", "CREATE", fpath);
        modularize_directory(fpath);
    }

    return res;
}

static int fs_rename(const char *oldpath, const char *newpath) {
    int res = 0;
    char old_fpath[256];
    char new_fpath[256];

    snprintf(old_fpath, sizeof(old_fpath), "%s%s", fs_path, oldpath);
    snprintf(new_fpath, sizeof(new_fpath), "%s%s", fs_path, newpath);

    res = rename(old_fpath, new_fpath);
    if (res == -1)
        return -errno;

    if ((strstr(oldpath, "module_") != NULL) || (strstr(newpath, "module_") != NULL)) {
        log_system_call("REPORT", "RENAME", strcat(strcat(old_fpath, "::"), new_fpath));
        modularize_directory(new_fpath);
    }

    return res;
}

static int fs_rmdir(const char *path) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    log_system_call("FLAG", "RMDIR", fpath);

    return res;
}

static int fs_unlink(const char *path) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = unlink(fpath);
    if (res == -1)
        return -errno;

    log_system_call("FLAG", "UNLINK", fpath);

    return res;
}

//fungsi dasar dari fuse
static struct fuse_operations fs_operations = {
    .getattr = fs_getattr, //mengambil atribut dari suatu file atau directory
    .readdir = fs_readdir, //mambaca isi dari sebuah directory
    .mkdir = fs_mkdir, //membuat directory baru
    .rename = fs_rename, //mengganti nama sebuah directory atau file
    .rmdir = fs_rmdir, //hapus directory
    .unlink = fs_unlink, //hapus file
};

int main(int argc, char *argv[]) {
    return fuse_main(argc, argv, &fs_operations, NULL); //memanggil fungsi fuse
}
