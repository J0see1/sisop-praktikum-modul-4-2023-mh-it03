#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <openssl/sha.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

#ifndef DT_REG
#define DT_REG 8
#endif
#define MAX_PASSWORD_LENGTH 100
#define MAX_FOLDER_LENGTH 256
#define LOG_FILE "logs-fuse.log"
#define MAX_LOG_LENGTH 200
#define PASSWORD_FILE "data/disable-area/password.txt"
#define MAX_ATTEMPTS 3
#define PATH_MAX 4096

// Deklarasi fungsi
void logData(const char *status, const char *tag, const char *information);
void createDirectory(const char *folderPath);
void reverseFileName(char *fileName);
void reverseIsiFile(const char *fileName);
char* base64_decode(const char* input);
char* rot13_decode(const char* input);
char* hex_decode(const char* input);
char* reverse_text(const char* input);
void lock_folder(const char *folder_path);
void mark_folder_locked();
void set_folder_password();
int access_folder_disable_area(const char *folder_path);
void folderGallery(char *galleryPath, int *result);
void folderSisop(char *sisopPath, int *result);
void folderTulisan (int *result);
void folderData(int *result);

static const char *base_path = "/home/angel/sisop/Sisop_Modul_4/nomer1/data"; // Set the base path

void folderGallery(char *galleryPath, int *result)
{
       createDirectory("data/gallery");
       createDirectory("data/gallery/rev");
       createDirectory("data/gallery/delete");

       int galleryOption;
       printf("Pilihan folder:\n");
       printf("1. Buat folder 'rev' untuk membalikkan nama file gambar\n");
       printf("2. Buat folder 'delete' untuk menyimpan file gambar\n");
       printf("Pilihan Anda: ");
       scanf("%d", &galleryOption);

       if (galleryOption == 1) {
           char fileName[256];
           printf("Masukkan nama file gambar: ");
           scanf("%s", fileName);

           char revFileName[256] = "data/gallery/rev/";
           strcat(revFileName, fileName);

           reverseFileName(fileName);
           printf("Nama file dibalikkan: %s\n", fileName);

           FILE *revFile = fopen(revFileName, "w");
           if (revFile == NULL) {
               printf("File tidak dapat dibuka untuk penulisan.\n");
          
           }
           fprintf(revFile, "%s", fileName);
           fclose(revFile);

           logData("SUCCESS", "GALLERY_OPERATION", "Reversed file name in gallery");

       } else if (galleryOption == 2) {
       char fileName[256];
       printf("Masukkan nama file gambar: ");
       scanf("%s", fileName);

       char deleteFileName[256] = "data/gallery/delete/";
       strcat(deleteFileName, fileName);

       FILE *deleteFile = fopen(deleteFileName, "w");
       if (deleteFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");

         }
       fclose(deleteFile);
       logData("SUCCESS", "GALLERY_OPERATION", "Deleted file in gallery");
       } else {
           printf("Pilihan tidak valid.\n");

           logData("FAILED", "GALLERY_OPERATION", "Invalid option selected");

       }
       *result = 0;
}

void folderSisop (char *sisopPath, int *result)
{
   char filePath[256] = "data/sisop/";
   int choice;

   printf("Menu:\n");
   printf("1. Ubah izin pada script.sh\n");
   printf("2. Reverse isi file dalam folder sisop\n");
   printf("Pilih menu (1/2): ");
   scanf("%d", &choice);

   if (choice == 1) {
       // Menu 1: Ubah izin pada script.sh
       if (system("chmod u+x script.sh") == -1) {
           perror("chmod");
       }
       logData("SUCCESS", "SISOP_OPERATION", "Permission changed on script.sh");
   } else if (choice == 2) {

       char fileName[256];
       printf("Masukkan nama file yang ingin diubah: ");
       scanf("%s", fileName);

       strcat(filePath, fileName);

       reverseIsiFile(filePath);

       FILE *fileAsli = fopen(filePath, "r");
       if (fileAsli == NULL) {
           printf("File tidak dapat dibuka.\n");
  
       }

       fseek(fileAsli, 0, SEEK_END);
       long fileAsliSize = ftell(fileAsli);
       fseek(fileAsli, 0, SEEK_SET);

       char *isiAsli = (char *)malloc(fileAsliSize + 1);
       fread(isiAsli, fileAsliSize, 1, fileAsli);
       isiAsli[fileAsliSize] = '\0';
       fclose(fileAsli);

       printf("Isi file sebelum perubahan:\n%s\n", isiAsli);
       free(isiAsli);

       printf("Apakah Anda ingin menyimpan perubahan (y/n)? ");
       char choice;
       scanf(" %c", &choice);

       if (choice == 'y' || choice == 'Y') {
       logData("SUCCESS", "SISOP_OPERATION", "File changes saved in Sisop folder");
       } else if (choice == 'n' || choice == 'N') {
           printf("Perubahan tidak disimpan.\n");
       logData("SUCCESS", "SISOP_OPERATION", "File changes discarded in Sisop folder");
       } logData("SUCCESS", "SISOP_OPERATION", "File content reversed in Sisop folder");
   } else {
       printf("Pilihan tidak valid.\n");
       logData("FAILED", "SISOP_OPERATION", "Invalid menu selection");
   }
   *result = 0;
}

void folderTulisan (int *result)
{
   char filePath[256] = "data/tulisan/";
       char fileName[256];
       printf("Masukkan nama file yang ingin di-decode: ");
       scanf("%255s", fileName);

       const char *path = "data/tulisan";
       char full_path[512];

       snprintf(full_path, sizeof(full_path), "%s/%s", path, fileName);

       FILE *file = fopen(full_path, "r");
       if (file) {
           fseek(file, 0, SEEK_END);
           long file_size = ftell(file);
           rewind(file);

           char *isi_file = (char *)malloc(file_size + 1);
           fread(isi_file, 1, file_size, file);
           isi_file[file_size] = '\0';

           printf("Isi file sebelum decode:\n%s\n", isi_file);

           char *decoded_content = NULL;

           if (strncmp(fileName, "notes-base64", 11) == 0 || strncmp(fileName, "coba-base64", 10) == 0) {
               decoded_content = base64_decode(isi_file);
           } else if (strncmp(fileName, "enkripsi_rot13", 13) == 0) {
               decoded_content = rot13_decode(isi_file);
           } else if (strncmp(fileName, "new-hex", 7) == 0 || strncmp(fileName, "text-hex", 8) == 0) {
               decoded_content = hex_decode(isi_file);
           } else if (strncmp(fileName, "rev-text", 8) == 0) {
               decoded_content = reverse_text(isi_file);

               logData("SUCCESS", "TULISAN_OPERATION", "File content decoded in Tulisan folder");
           } else {
               printf("Prefix file tidak valid untuk operasi decode yang diimplementasikan.\n");
               free(isi_file);
               fclose(file);
           }

           printf("\nIsi file setelah decode:\n%s\n", decoded_content);

           free(isi_file);
           free(decoded_content);
           fclose(file);
       } else {
           printf("File tidak ditemukan.\n");

           logData("FAILED", "TULISAN_OPERATION", "File content decode failed in Tulisan folder");
       }
       *result = 0;
}

void folderData (int *result)
{
   const char *folder_path = "data";
       DIR *dir;
       struct dirent *ent;

       if ((dir = opendir(folder_path)) != NULL) {
           while ((ent = readdir(dir)) != NULL) {
               if (strncmp(ent->d_name, "disable-area", 12) == 0) {
                   lock_folder("data/disable-area");
                   mark_folder_locked();
               }
           }
           closedir(dir);
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           perror("Error opening directory");
       }

       if (access(PASSWORD_FILE, F_OK) == -1) {
           // Jika password belum diatur, set password
           set_folder_password();
       }

       access_folder_disable_area("data/disable-area");

       logData("FAILED", "DISABLE_AREA_OPERATION", "Password file not found");

       *result = 0;
   }
  
void reverseString(char *str, int len) {
   int start = 0;
   int end = len - 1;

   while (start < end) {
       char temp = str[start];
       str[start] = str[end];
       str[end] = temp;
       start++;
       end--;
   }
}

void createDirectory(const char *folderPath) {
   struct stat st = {0};
   if (stat(folderPath, &st) == -1) {
       if (mkdir(folderPath, 0700) != 0) {
           printf("Gagal membuat direktori %s.\n", folderPath);
           exit(EXIT_FAILURE);
       }
   }
}

void reverseFileName(char *fileName) {
   char *dot = strrchr(fileName, '.');
   int len = (dot == NULL) ? strlen(fileName) : (dot - fileName);
   char extension[10] = "";
   if (dot != NULL) {
       strcpy(extension, dot);
       *dot = '\0';
   }
   reverseString(fileName, len);
   if (dot != NULL) {
       strcat(fileName, extension);

       printf("Membalik nama file: %s\n", fileName);
   }
}

void reverseIsiFile(const char *fileName) {
   FILE *file = fopen(fileName, "rb");
   if (file == NULL) {
       printf("File tidak ditemukan atau tidak dapat dibuka.\n");
       return;
   }

   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);

   char *isiFile = (char *)malloc(fileSize + 1);
   fread(isiFile, fileSize, 1, file);
   isiFile[fileSize] = '\0';
   fclose(file);

   reverseString(isiFile, fileSize);
   FILE *reverseFile = fopen(fileName, "wb");
   if (reverseFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");
       free(isiFile);
       return;
   }
   fwrite(isiFile, fileSize, 1, reverseFile);
   fclose(reverseFile);
   free(isiFile);
   printf("Isi file %s telah dibalikkan.\n", fileName);
}

char* base64_decode(const char* input) {
   BIO *bio, *b64, *bio_out;

 int decoded_size = 0;

   int input_length = strlen(input);
   int max_decoded_length = (input_length * 3) / 4; // Estimasi panjang maksimal setelah decode
   char* buffer = (char*)malloc(max_decoded_length);

   b64 = BIO_new(BIO_f_base64());
   bio = BIO_new_mem_buf((void*)input, -1);
   bio = BIO_push(b64, bio);

   bio_out = BIO_new(BIO_s_mem());
   BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

   decoded_size = BIO_read(bio, buffer, input_length);
   buffer[decoded_size] = '\0';

   BIO_free_all(bio);

   return buffer;
}

char* rot13_decode(const char* input) {
   char* result = strdup(input);
   if (result == NULL) {
       return NULL;
   }
   for (int i = 0; result[i] != '\0'; i++) {
       if (isalpha(result[i])) {
           char base = islower(result[i]) ? 'a' : 'A';
           result[i] = (result[i] - base + 13) % 26 + base;
       }
   }
   return result;
}

char* hex_decode(const char* input) {
   int input_length = strlen(input);
   if (input_length % 2 != 0) {
       return NULL;
   }
   int output_length = input_length / 2;
   char* buffer = (char*)malloc(output_length + 1);
   if (buffer == NULL) {
       return NULL;
   }
   for (int i = 0; i < output_length; i++) {
       int high = input[i * 2];
       int low = input[i * 2 + 1];
       if (isxdigit(high) && isxdigit(low)) {
           high = isdigit(high) ? high - '0' : (toupper(high) - 'A') + 10;
           low = isdigit(low) ? low - '0' : (toupper(low) - 'A') + 10;
           buffer[i] = (high << 4) | low;
       } else {
           free(buffer);
           return NULL;
       }
   }
   buffer[output_length] = '\0';
   return buffer;
}

char* reverse_text(const char* input) {
   int length = strlen(input);
   char* reversed = (char*)malloc(length + 1);
   if (reversed == NULL) {
       return NULL;
   }
   for (int i = 0; i < length; i++) {
       reversed[i] = input[length - i - 1];
   }
   reversed[length] = '\0';
   return reversed;
}

char folderPassword[MAX_PASSWORD_LENGTH];

void lock_folder(const char *folder_path) {
   const char *path = "data/disable-area";
   // Mengecek apakah folder sudah ada atau belum
   struct stat st = {0};
   if (stat(folder_path, &st) == -1) {
       // Jika folder belum ada, maka buat folder
       if (mkdir(folder_path, 0700) == -1) {
           perror("Error creating folder");
  
       } else {
           printf("Folder created successfully: %s\n", folder_path);
       }
   } else {
       printf("Folder already exists: %s\n", folder_path);
   }
   // Mengunci akses folder
   if (chmod(folder_path, 0500) == -1) {
       perror("Error changing folder permissions");

   } else {
       printf("Folder locked successfully: %s\n", folder_path);
   }
}

void mark_folder_locked() {
   FILE *statusFile = fopen("kunci_status.txt", "w");
   if (statusFile != NULL) {
       fprintf(statusFile, "Folder ini telah dikunci.\n");
       fclose(statusFile);
   } else {
       perror("Error opening status file");
       exit(EXIT_FAILURE);
   }
}

void set_folder_password() {
   char password[50];
   printf("Buat password untuk folder disable-area: ");
   fgets(password, sizeof(password), stdin);

   FILE *file = fopen(PASSWORD_FILE, "w");
   if (file == NULL) {
       perror("Error creating password file");
       exit(EXIT_FAILURE);
   }
   fprintf(file, "%s", password);
   fclose(file);
}

int authenticate_user() {
   char entered_password[50];

   for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt) {
       printf("Masukkan password untuk mengakses folder disable-area (Percobaan %d/%d): ", attempt, MAX_ATTEMPTS);
       fgets(entered_password, sizeof(entered_password), stdin);

       FILE *file = fopen(PASSWORD_FILE, "r");
       if (file == NULL) {
           perror("Error reading password file");
           exit(EXIT_FAILURE);
       }
       char correct_password[50];
       fgets(correct_password, sizeof(correct_password), file);
       fclose(file);

       if (strcmp(entered_password, correct_password) == 0) {
           return 1; // Password benar, izin diberikan
       } else {
           printf("Password salah. ");
       }
   }

   return 0; // Melebihi batas percobaan, izin ditolak
}

int access_folder_disable_area(const char *folder_path) {
   DIR *dir;
   struct dirent *ent;

   if ((dir = opendir(folder_path)) != NULL) {
       int success = 0;

       while ((ent = readdir(dir)) != NULL) {
           if (strncmp(ent->d_name, "disable-area", 12) == 0) {
               success = 1;
               break;
           }
       }
       closedir(dir);

       if (success) {
           // Folder ditemukan, lakukan operasi yang diperlukan
           lock_folder(folder_path);
           mark_folder_locked();
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           // Folder tidak ditemukan
           logData("FAILED", "DISABLE_AREA_OPERATION", "Folder not found");
       }
       return success;  // Mengemreverse status keberhasilan
   } else {
       // Terjadi kesalahan saat membuka direktori
       perror("Error opening directory");
       logData("FAILED", "DISABLE_AREA_OPERATION", "Error opening directory");

       return -1;  // Mengemreverse status kesalahan
   }
}
void logData(const char *status, const char *tag, const char *information) {
   FILE *log = fopen(LOG_FILE, "a");
   if (log != NULL) {
       time_t rawtime;
       struct tm *timeinfo;
       time(&rawtime);
       timeinfo = localtime(&rawtime);

       char timestamp[MAX_LOG_LENGTH];
       strftime(timestamp, MAX_LOG_LENGTH, "[%d/%m/%Y-%H:%M:%S]", timeinfo);

       fprintf(log, "[%s]::%s::[%s]::[%s]\n", status, timestamp, tag, information);
       fclose(log);
   } else {
       printf("Tidak dapat membuka file log untuk penulisan.\n");
   }
}

static int xmp_fullpath(char *fullpath, const char *path) {
   strcpy(fullpath, base_path);
   strncat(fullpath, path, PATH_MAX);
   return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
   int res;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   res = lstat(fullpath, stbuf);

   if (res == -1)
       return -errno;

   return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
   DIR *dp;
   struct dirent *de;
   (void)offset;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   dp = opendir(fullpath);

   if (dp == NULL)
       return -errno;

   while ((de = readdir(dp)) != NULL) {
       struct stat st;

       memset(&st, 0, sizeof(st));

       st.st_ino = de->d_ino;
       st.st_mode = de->d_type << 12;

       if (filler(buf, de->d_name, &st, 0))
           break;
   }
   closedir(dp);
   return 0;
}
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
   int fd;
   int res;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   fd = open(fullpath, O_RDONLY);

   if (fd == -1)
       return -errno;

   res = pread(fd, buf, size, offset);

   if (res == -1)
       res = -errno;

   close(fd);

   return res;
}
static struct fuse_operations xmp_oper = {
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,
};
// Function to run FUSE in a separate process
void run_fuse() {
   char *fuse_argv[] = {"hell", "/home/angel/sisop/Sisop_Modul_4/mount", "-f", NULL}; // Adjust the mount point accordingly
   int fuse_argc = sizeof(fuse_argv) / sizeof(fuse_argv[0]) - 1;

   fuse_main(fuse_argc, fuse_argv, &xmp_oper, NULL);
}
// pthread_t fuse_thread;

int main(int argc, char *argv[]) {
   umask(0);

   pid_t fuse_pid;

   // Create a process to run the FUSE file system
   if ((fuse_pid = fork()) == -1) {
       fprintf(stderr, "Error creating FUSE process\n");
       return 1;
   }

   if (fuse_pid == 0) {
       // This is the child process (FUSE)
       run_fuse();
       exit(0);
   }

//    pthread_create(&fuse_thread, NULL, fuse_thread_function, NULL);
//    pthread_join(fuse_thread, NULL);
   int result;

   int folderChoice;
   do {
       printf("Pilih folder:\n");
       printf("1. Gallery\n");
       printf("2. Sisop\n");
       printf("3. Tulisan\n");
       printf("4. Data\n");
       printf("Pilihan Anda: ");
       scanf("%d", &folderChoice);

       // Clear the input buffer
       int c;
       while ((c = getchar()) != '\n' && c != EOF);

       switch (folderChoice) {
       case 1:
           folderGallery("data/gallery", &result);
           break;
       case 2:
           folderSisop("data/sisop", &result);
           break;
       case 3:
           folderTulisan(&result);

           break;
       case 4:
           folderData(&result);
           break;
       default:
           printf("Pilihan tidak valid. Coba lagi.\n");
   }
} while (1);

   int status;
   waitpid(fuse_pid, &status, 0);

//    pthread_join(fuse_thread, NULL);

   return 0;
}
