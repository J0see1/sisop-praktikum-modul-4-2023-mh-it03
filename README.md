# sisop-praktikum-modul-4-2023-mh-it03

Pengerjaan soal shift sistem operasi modul 4 oleh IT03

# Anggota

| Nama                            | NRP          |
| ------------------------------- | ------------ |
| Marcelinus Alvinanda Chrisantya | `5027221012` |
| George David Nebore             | `5027221043` |
| Angella Christie                | `5027221047` |

# Konten

- [Soal 1](#soal-1)

- [Soal 2](#soal-2)

- [Soal 3](#soal-3)

- [Soal 4](#soal-4)

# Soal 1
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada https://drive.google.com/file/d/1ci9afRbwJi-jrly7CB7HiC40rsuvSa03/view?usp=drive_link.

- Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:
    - Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
			Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
            Output: eTgj80q7Ut7eNhNVooBE.HEIC
    - Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus. Ex: "mv coba-deh.jpg delete-foto/" 
- Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 
    - Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
    - Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  
- Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
    - Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.
    - Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.
    - Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.
    - Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.
- Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 
    - Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 
- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format : [SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information] 
    Ex: [SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6

# hell.c 

``` 
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <openssl/sha.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

#ifndef DT_REG
#define DT_REG 8
#endif
#define MAX_PASSWORD_LENGTH 100
#define MAX_FOLDER_LENGTH 256
#define LOG_FILE "logs-fuse.log"
#define MAX_LOG_LENGTH 200
#define PASSWORD_FILE "data/disable-area/password.txt"
#define MAX_ATTEMPTS 3
#define PATH_MAX 4096

// Deklarasi fungsi
void logData(const char *status, const char *tag, const char *information);
void createDirectory(const char *folderPath);
void reverseFileName(char *fileName);
void reverseIsiFile(const char *fileName);
char* base64_decode(const char* input);
char* rot13_decode(const char* input);
char* hex_decode(const char* input);
char* reverse_text(const char* input);
void lock_folder(const char *folder_path);
void mark_folder_locked();
void set_folder_password();
int access_folder_disable_area(const char *folder_path);
void folderGallery(char *galleryPath, int *result);
void folderSisop(char *sisopPath, int *result);
void folderTulisan (int *result);
void folderData(int *result);

static const char *base_path = "/home/angel/sisop/Sisop_Modul_4/nomer1/data"; // Set the base path

void folderGallery(char *galleryPath, int *result)
{
       createDirectory("data/gallery");
       createDirectory("data/gallery/rev");
       createDirectory("data/gallery/delete");

       int galleryOption;
       printf("Pilihan folder:\n");
       printf("1. Buat folder 'rev' untuk membalikkan nama file gambar\n");
       printf("2. Buat folder 'delete' untuk menyimpan file gambar\n");
       printf("Pilihan Anda: ");
       scanf("%d", &galleryOption);

       if (galleryOption == 1) {
           char fileName[256];
           printf("Masukkan nama file gambar: ");
           scanf("%s", fileName);

           char revFileName[256] = "data/gallery/rev/";
           strcat(revFileName, fileName);

           reverseFileName(fileName);
           printf("Nama file dibalikkan: %s\n", fileName);

           FILE *revFile = fopen(revFileName, "w");
           if (revFile == NULL) {
               printf("File tidak dapat dibuka untuk penulisan.\n");
          
           }
           fprintf(revFile, "%s", fileName);
           fclose(revFile);

           logData("SUCCESS", "GALLERY_OPERATION", "Reversed file name in gallery");

       } else if (galleryOption == 2) {
       char fileName[256];
       printf("Masukkan nama file gambar: ");
       scanf("%s", fileName);

       char deleteFileName[256] = "data/gallery/delete/";
       strcat(deleteFileName, fileName);

       FILE *deleteFile = fopen(deleteFileName, "w");
       if (deleteFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");

         }
       fclose(deleteFile);
       logData("SUCCESS", "GALLERY_OPERATION", "Deleted file in gallery");
       } else {
           printf("Pilihan tidak valid.\n");

           logData("FAILED", "GALLERY_OPERATION", "Invalid option selected");

       }
       *result = 0;
}

void folderSisop (char *sisopPath, int *result)
{
   char filePath[256] = "data/sisop/";
   int choice;

   printf("Menu:\n");
   printf("1. Ubah izin pada script.sh\n");
   printf("2. Reverse isi file dalam folder sisop\n");
   printf("Pilih menu (1/2): ");
   scanf("%d", &choice);

   if (choice == 1) {
       // Menu 1: Ubah izin pada script.sh
       if (system("chmod u+x script.sh") == -1) {
           perror("chmod");
       }
       logData("SUCCESS", "SISOP_OPERATION", "Permission changed on script.sh");
   } else if (choice == 2) {

       char fileName[256];
       printf("Masukkan nama file yang ingin diubah: ");
       scanf("%s", fileName);

       strcat(filePath, fileName);

       reverseIsiFile(filePath);

       FILE *fileAsli = fopen(filePath, "r");
       if (fileAsli == NULL) {
           printf("File tidak dapat dibuka.\n");
  
       }

       fseek(fileAsli, 0, SEEK_END);
       long fileAsliSize = ftell(fileAsli);
       fseek(fileAsli, 0, SEEK_SET);

       char *isiAsli = (char *)malloc(fileAsliSize + 1);
       fread(isiAsli, fileAsliSize, 1, fileAsli);
       isiAsli[fileAsliSize] = '\0';
       fclose(fileAsli);

       printf("Isi file sebelum perubahan:\n%s\n", isiAsli);
       free(isiAsli);

       printf("Apakah Anda ingin menyimpan perubahan (y/n)? ");
       char choice;
       scanf(" %c", &choice);

       if (choice == 'y' || choice == 'Y') {
       logData("SUCCESS", "SISOP_OPERATION", "File changes saved in Sisop folder");
       } else if (choice == 'n' || choice == 'N') {
           printf("Perubahan tidak disimpan.\n");
       logData("SUCCESS", "SISOP_OPERATION", "File changes discarded in Sisop folder");
       } logData("SUCCESS", "SISOP_OPERATION", "File content reversed in Sisop folder");
   } else {
       printf("Pilihan tidak valid.\n");
       logData("FAILED", "SISOP_OPERATION", "Invalid menu selection");
   }
   *result = 0;
}

void folderTulisan (int *result)
{
   char filePath[256] = "data/tulisan/";
       char fileName[256];
       printf("Masukkan nama file yang ingin di-decode: ");
       scanf("%255s", fileName);

       const char *path = "data/tulisan";
       char full_path[512];

       snprintf(full_path, sizeof(full_path), "%s/%s", path, fileName);

       FILE *file = fopen(full_path, "r");
       if (file) {
           fseek(file, 0, SEEK_END);
           long file_size = ftell(file);
           rewind(file);

           char *isi_file = (char *)malloc(file_size + 1);
           fread(isi_file, 1, file_size, file);
           isi_file[file_size] = '\0';

           printf("Isi file sebelum decode:\n%s\n", isi_file);

           char *decoded_content = NULL;

           if (strncmp(fileName, "notes-base64", 11) == 0 || strncmp(fileName, "coba-base64", 10) == 0) {
               decoded_content = base64_decode(isi_file);
           } else if (strncmp(fileName, "enkripsi_rot13", 13) == 0) {
               decoded_content = rot13_decode(isi_file);
           } else if (strncmp(fileName, "new-hex", 7) == 0 || strncmp(fileName, "text-hex", 8) == 0) {
               decoded_content = hex_decode(isi_file);
           } else if (strncmp(fileName, "rev-text", 8) == 0) {
               decoded_content = reverse_text(isi_file);

               logData("SUCCESS", "TULISAN_OPERATION", "File content decoded in Tulisan folder");
           } else {
               printf("Prefix file tidak valid untuk operasi decode yang diimplementasikan.\n");
               free(isi_file);
               fclose(file);
           }

           printf("\nIsi file setelah decode:\n%s\n", decoded_content);

           free(isi_file);
           free(decoded_content);
           fclose(file);
       } else {
           printf("File tidak ditemukan.\n");

           logData("FAILED", "TULISAN_OPERATION", "File content decode failed in Tulisan folder");
       }
       *result = 0;
}

void folderData (int *result)
{
   const char *folder_path = "data";
       DIR *dir;
       struct dirent *ent;

       if ((dir = opendir(folder_path)) != NULL) {
           while ((ent = readdir(dir)) != NULL) {
               if (strncmp(ent->d_name, "disable-area", 12) == 0) {
                   lock_folder("data/disable-area");
                   mark_folder_locked();
               }
           }
           closedir(dir);
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           perror("Error opening directory");
       }

       if (access(PASSWORD_FILE, F_OK) == -1) {
           // Jika password belum diatur, set password
           set_folder_password();
       }

       access_folder_disable_area("data/disable-area");

       logData("FAILED", "DISABLE_AREA_OPERATION", "Password file not found");

       *result = 0;
   }
  
void reverseString(char *str, int len) {
   int start = 0;
   int end = len - 1;

   while (start < end) {
       char temp = str[start];
       str[start] = str[end];
       str[end] = temp;
       start++;
       end--;
   }
}

void createDirectory(const char *folderPath) {
   struct stat st = {0};
   if (stat(folderPath, &st) == -1) {
       if (mkdir(folderPath, 0700) != 0) {
           printf("Gagal membuat direktori %s.\n", folderPath);
           exit(EXIT_FAILURE);
       }
   }
}

void reverseFileName(char *fileName) {
   char *dot = strrchr(fileName, '.');
   int len = (dot == NULL) ? strlen(fileName) : (dot - fileName);
   char extension[10] = "";
   if (dot != NULL) {
       strcpy(extension, dot);
       *dot = '\0';
   }
   reverseString(fileName, len);
   if (dot != NULL) {
       strcat(fileName, extension);

       printf("Membalik nama file: %s\n", fileName);
   }
}

void reverseIsiFile(const char *fileName) {
   FILE *file = fopen(fileName, "rb");
   if (file == NULL) {
       printf("File tidak ditemukan atau tidak dapat dibuka.\n");
       return;
   }

   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);

   char *isiFile = (char *)malloc(fileSize + 1);
   fread(isiFile, fileSize, 1, file);
   isiFile[fileSize] = '\0';
   fclose(file);

   reverseString(isiFile, fileSize);
   FILE *reverseFile = fopen(fileName, "wb");
   if (reverseFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");
       free(isiFile);
       return;
   }
   fwrite(isiFile, fileSize, 1, reverseFile);
   fclose(reverseFile);
   free(isiFile);
   printf("Isi file %s telah dibalikkan.\n", fileName);
}

char* base64_decode(const char* input) {
   BIO *bio, *b64, *bio_out;

 int decoded_size = 0;

   int input_length = strlen(input);
   int max_decoded_length = (input_length * 3) / 4; // Estimasi panjang maksimal setelah decode
   char* buffer = (char*)malloc(max_decoded_length);

   b64 = BIO_new(BIO_f_base64());
   bio = BIO_new_mem_buf((void*)input, -1);
   bio = BIO_push(b64, bio);

   bio_out = BIO_new(BIO_s_mem());
   BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

   decoded_size = BIO_read(bio, buffer, input_length);
   buffer[decoded_size] = '\0';

   BIO_free_all(bio);

   return buffer;
}

char* rot13_decode(const char* input) {
   char* result = strdup(input);
   if (result == NULL) {
       return NULL;
   }
   for (int i = 0; result[i] != '\0'; i++) {
       if (isalpha(result[i])) {
           char base = islower(result[i]) ? 'a' : 'A';
           result[i] = (result[i] - base + 13) % 26 + base;
       }
   }
   return result;
}

char* hex_decode(const char* input) {
   int input_length = strlen(input);
   if (input_length % 2 != 0) {
       return NULL;
   }
   int output_length = input_length / 2;
   char* buffer = (char*)malloc(output_length + 1);
   if (buffer == NULL) {
       return NULL;
   }
   for (int i = 0; i < output_length; i++) {
       int high = input[i * 2];
       int low = input[i * 2 + 1];
       if (isxdigit(high) && isxdigit(low)) {
           high = isdigit(high) ? high - '0' : (toupper(high) - 'A') + 10;
           low = isdigit(low) ? low - '0' : (toupper(low) - 'A') + 10;
           buffer[i] = (high << 4) | low;
       } else {
           free(buffer);
           return NULL;
       }
   }
   buffer[output_length] = '\0';
   return buffer;
}

char* reverse_text(const char* input) {
   int length = strlen(input);
   char* reversed = (char*)malloc(length + 1);
   if (reversed == NULL) {
       return NULL;
   }
   for (int i = 0; i < length; i++) {
       reversed[i] = input[length - i - 1];
   }
   reversed[length] = '\0';
   return reversed;
}

char folderPassword[MAX_PASSWORD_LENGTH];

void lock_folder(const char *folder_path) {
   const char *path = "data/disable-area";
   // Mengecek apakah folder sudah ada atau belum
   struct stat st = {0};
   if (stat(folder_path, &st) == -1) {
       // Jika folder belum ada, maka buat folder
       if (mkdir(folder_path, 0700) == -1) {
           perror("Error creating folder");
  
       } else {
           printf("Folder created successfully: %s\n", folder_path);
       }
   } else {
       printf("Folder already exists: %s\n", folder_path);
   }
   // Mengunci akses folder
   if (chmod(folder_path, 0500) == -1) {
       perror("Error changing folder permissions");

   } else {
       printf("Folder locked successfully: %s\n", folder_path);
   }
}

void mark_folder_locked() {
   FILE *statusFile = fopen("kunci_status.txt", "w");
   if (statusFile != NULL) {
       fprintf(statusFile, "Folder ini telah dikunci.\n");
       fclose(statusFile);
   } else {
       perror("Error opening status file");
       exit(EXIT_FAILURE);
   }
}

void set_folder_password() {
   char password[50];
   printf("Buat password untuk folder disable-area: ");
   fgets(password, sizeof(password), stdin);

   FILE *file = fopen(PASSWORD_FILE, "w");
   if (file == NULL) {
       perror("Error creating password file");
       exit(EXIT_FAILURE);
   }
   fprintf(file, "%s", password);
   fclose(file);
}

int authenticate_user() {
   char entered_password[50];

   for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt) {
       printf("Masukkan password untuk mengakses folder disable-area (Percobaan %d/%d): ", attempt, MAX_ATTEMPTS);
       fgets(entered_password, sizeof(entered_password), stdin);

       FILE *file = fopen(PASSWORD_FILE, "r");
       if (file == NULL) {
           perror("Error reading password file");
           exit(EXIT_FAILURE);
       }
       char correct_password[50];
       fgets(correct_password, sizeof(correct_password), file);
       fclose(file);

       if (strcmp(entered_password, correct_password) == 0) {
           return 1; // Password benar, izin diberikan
       } else {
           printf("Password salah. ");
       }
   }

   return 0; // Melebihi batas percobaan, izin ditolak
}

int access_folder_disable_area(const char *folder_path) {
   DIR *dir;
   struct dirent *ent;

   if ((dir = opendir(folder_path)) != NULL) {
       int success = 0;

       while ((ent = readdir(dir)) != NULL) {
           if (strncmp(ent->d_name, "disable-area", 12) == 0) {
               success = 1;
               break;
           }
       }
       closedir(dir);

       if (success) {
           // Folder ditemukan, lakukan operasi yang diperlukan
           lock_folder(folder_path);
           mark_folder_locked();
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           // Folder tidak ditemukan
           logData("FAILED", "DISABLE_AREA_OPERATION", "Folder not found");
       }
       return success;  // Mengemreverse status keberhasilan
   } else {
       // Terjadi kesalahan saat membuka direktori
       perror("Error opening directory");
       logData("FAILED", "DISABLE_AREA_OPERATION", "Error opening directory");

       return -1;  // Mengemreverse status kesalahan
   }
}
void logData(const char *status, const char *tag, const char *information) {
   FILE *log = fopen(LOG_FILE, "a");
   if (log != NULL) {
       time_t rawtime;
       struct tm *timeinfo;
       time(&rawtime);
       timeinfo = localtime(&rawtime);

       char timestamp[MAX_LOG_LENGTH];
       strftime(timestamp, MAX_LOG_LENGTH, "[%d/%m/%Y-%H:%M:%S]", timeinfo);

       fprintf(log, "[%s]::%s::[%s]::[%s]\n", status, timestamp, tag, information);
       fclose(log);
   } else {
       printf("Tidak dapat membuka file log untuk penulisan.\n");
   }
}

static int xmp_fullpath(char *fullpath, const char *path) {
   strcpy(fullpath, base_path);
   strncat(fullpath, path, PATH_MAX);
   return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
   int res;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   res = lstat(fullpath, stbuf);

   if (res == -1)
       return -errno;

   return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
   DIR *dp;
   struct dirent *de;
   (void)offset;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   dp = opendir(fullpath);

   if (dp == NULL)
       return -errno;

   while ((de = readdir(dp)) != NULL) {
       struct stat st;

       memset(&st, 0, sizeof(st));

       st.st_ino = de->d_ino;
       st.st_mode = de->d_type << 12;

       if (filler(buf, de->d_name, &st, 0))
           break;
   }
   closedir(dp);
   return 0;
}
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
   int fd;
   int res;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   fd = open(fullpath, O_RDONLY);

   if (fd == -1)
       return -errno;

   res = pread(fd, buf, size, offset);

   if (res == -1)
       res = -errno;

   close(fd);

   return res;
}
static struct fuse_operations xmp_oper = {
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,
};
// Function to run FUSE in a separate process
void run_fuse() {
   char *fuse_argv[] = {"hell", "/home/angel/sisop/Sisop_Modul_4/mount", "-f", NULL}; // Adjust the mount point accordingly
   int fuse_argc = sizeof(fuse_argv) / sizeof(fuse_argv[0]) - 1;

   fuse_main(fuse_argc, fuse_argv, &xmp_oper, NULL);
}
pthread_t fuse_thread;

int main(int argc, char *argv[]) {
   umask(0);

   pid_t fuse_pid;

   // Create a process to run the FUSE file system
   if ((fuse_pid = fork()) == -1) {
       fprintf(stderr, "Error creating FUSE process\n");
       return 1;
   }

   if (fuse_pid == 0) {
       // This is the child process (FUSE)
       run_fuse();
       exit(0);
   }

   pthread_create(&fuse_thread, NULL, fuse_thread_function, NULL);
   pthread_join(fuse_thread, NULL);
   int result;

   int folderChoice;
   do {
       printf("Pilih folder:\n");
       printf("1. Gallery\n");
       printf("2. Sisop\n");
       printf("3. Tulisan\n");
       printf("4. Data\n");
       printf("Pilihan Anda: ");
       scanf("%d", &folderChoice);

       // Clear the input buffer
       int c;
       while ((c = getchar()) != '\n' && c != EOF);

       switch (folderChoice) {
       case 1:
           folderGallery("data/gallery", &result);
           break;
       case 2:
           folderSisop("data/sisop", &result);
           break;
       case 3:
           folderTulisan(&result);

           break;
       case 4:
           folderData(&result);
           break;
       default:
           printf("Pilihan tidak valid. Coba lagi.\n");
   }
} while (1);

   int status;
   waitpid(fuse_pid, &status, 0);

   pthread_join(fuse_thread, NULL);

   return 0;
}
``` 
# PENJELASAN PROGRAM
``` 
void logData(const char *status, const char *tag, const char *information);
void createDirectory(const char *folderPath);
void reverseFileName(char *fileName);
void reverseIsiFile(const char *fileName);
char* base64_decode(const char* input);
char* rot13_decode(const char* input);
char* hex_decode(const char* input);
char* reverse_text(const char* input);
void lock_folder(const char *folder_path);
void mark_folder_locked();
void set_folder_password();
int access_folder_disable_area(const char *folder_path);
void folderGallery(char *galleryPath, int *result);
void folderSisop(char *sisopPath, int *result);
void folderTulisan (int *result);
void folderData(int *result);
```
- code diatas digunakan untuk mendeklarasikan fungsi-fungsi yang digunakan pada program.

```
void folderGallery(char *galleryPath, int *result)
{
       createDirectory("data/gallery");
       createDirectory("data/gallery/rev");
       createDirectory("data/gallery/delete");

       int galleryOption;
       printf("Pilihan folder:\n");
       printf("1. Buat folder 'rev' untuk membalikkan nama file gambar\n");
       printf("2. Buat folder 'delete' untuk menyimpan file gambar\n");
       printf("Pilihan Anda: ");
       scanf("%d", &galleryOption);

       if (galleryOption == 1) {
           char fileName[256];
           printf("Masukkan nama file gambar: ");
           scanf("%s", fileName);

           char revFileName[256] = "data/gallery/rev/";
           strcat(revFileName, fileName);

           reverseFileName(fileName);
           printf("Nama file dibalikkan: %s\n", fileName);

           FILE *revFile = fopen(revFileName, "w");
           if (revFile == NULL) {
               printf("File tidak dapat dibuka untuk penulisan.\n");
          
           }
           fprintf(revFile, "%s", fileName);
           fclose(revFile);

           logData("SUCCESS", "GALLERY_OPERATION", "Reversed file name in gallery");

       } else if (galleryOption == 2) {
       char fileName[256];
       printf("Masukkan nama file gambar: ");
       scanf("%s", fileName);

       char deleteFileName[256] = "data/gallery/delete/";
       strcat(deleteFileName, fileName);

       FILE *deleteFile = fopen(deleteFileName, "w");
       if (deleteFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");

         }
       fclose(deleteFile);
       logData("SUCCESS", "GALLERY_OPERATION", "Deleted file in gallery");
       } else {
           printf("Pilihan tidak valid.\n");

           logData("FAILED", "GALLERY_OPERATION", "Invalid option selected");

       }
       *result = 0;
}
``` 
- Fungsi memanggil createDirectory tiga kali untuk membuat folder `"data/gallery", "data/gallery/rev", dan "data/gallery/delete"`.
- Menggunakan variabel `galleryOption` untuk meminta pengguna memilih operasi galeri yang diinginkan.
- Jika `galleryOption` sama dengan 1, pengguna diminta untuk memasukkan nama file gambar. Nama file tersebut dibalik menggunakan fungsi reverseFileName.
    - Nama file yang dibalik disimpan di `revFileName`.
    - File baru dengan nama tersebut dibuat dalam folder `"data/gallery/rev"`.
    - Informasi keberhasilan dicatat dalam log dengan memanggil fungsi `logData`.
- Jika `galleryOption` sama dengan 2, pengguna diminta untuk memasukkan nama file gambar.
    - Nama file tersebut ditambahkan ke path `"data/gallery/delete"`.
    - File baru dengan nama tersebut dibuat dalam folder `"data/gallery/delete"`.
    - Informasi keberhasilan dicatat dalam log dengan memanggil fungsi `logData`.
- Jika galleryOption tidak sama dengan 1 atau 2, cetak pesan bahwa pilihan tidak valid dan catat kegagalan dalam log.
```
void folderSisop (char *sisopPath, int *result)
{
   char filePath[256] = "data/sisop/";
   int choice;

   printf("Menu:\n");
   printf("1. Ubah izin pada script.sh\n");
   printf("2. Reverse isi file dalam folder sisop\n");
   printf("Pilih menu (1/2): ");
   scanf("%d", &choice);

   if (choice == 1) {
       // Menu 1: Ubah izin pada script.sh
       if (system("chmod u+x script.sh") == -1) {
           perror("chmod");
       }
       logData("SUCCESS", "SISOP_OPERATION", "Permission changed on script.sh");
   } else if (choice == 2) {

       char fileName[256];
       printf("Masukkan nama file yang ingin diubah: ");
       scanf("%s", fileName);

       strcat(filePath, fileName);

       reverseIsiFile(filePath);

       FILE *fileAsli = fopen(filePath, "r");
       if (fileAsli == NULL) {
           printf("File tidak dapat dibuka.\n");
  
       }

       fseek(fileAsli, 0, SEEK_END);
       long fileAsliSize = ftell(fileAsli);
       fseek(fileAsli, 0, SEEK_SET);

       char *isiAsli = (char *)malloc(fileAsliSize + 1);
       fread(isiAsli, fileAsliSize, 1, fileAsli);
       isiAsli[fileAsliSize] = '\0';
       fclose(fileAsli);

       printf("Isi file sebelum perubahan:\n%s\n", isiAsli);
       free(isiAsli);

       printf("Apakah Anda ingin menyimpan perubahan (y/n)? ");
       char choice;
       scanf(" %c", &choice);

       if (choice == 'y' || choice == 'Y') {
       logData("SUCCESS", "SISOP_OPERATION", "File changes saved in Sisop folder");
       } else if (choice == 'n' || choice == 'N') {
           printf("Perubahan tidak disimpan.\n");
       logData("SUCCESS", "SISOP_OPERATION", "File changes discarded in Sisop folder");
       } logData("SUCCESS", "SISOP_OPERATION", "File content reversed in Sisop folder");
   } else {
       printf("Pilihan tidak valid.\n");
       logData("FAILED", "SISOP_OPERATION", "Invalid menu selection");
   }
   *result = 0;
}
```
- `char filePath[256] = "data/sisop/";`: Menginisialisasi variabel `filePath` dengan string "data/sisop/".
- `int choice;`: Mendeklarasikan variabel choice untuk menyimpan pilihan menu pengguna.
- Menampilkan menu untuk pengguna dan meminta input pilihan menu.
- Jika pengguna memilih 1, maka izin pada file `"script.sh"` akan diubah menggunakan perintah `chmod u+x script.sh`.
- Jika perintah `system` untuk mengubah izin mengembalikan nilai -1, maka pesan kesalahan akan ditampilkan.
- Jika pengguna memilih 2, program akan meminta nama file (`fileName`) yang ingin diubah.
- Path lengkap file dibentuk dengan menggabungkan `filePath` dan `fileName`.
- Fungsi `reverseIsiFile` dipanggil untuk membalik isi file yang dipilih.
- Isi file sebelum perubahan ditampilkan.
- Pengguna diminta apakah ingin menyimpan perubahan. Jika ya, pesan log akan dicatat.
- Jika pengguna tidak ingin menyimpan perubahan, pesan log akan dicatat tetapi perubahan tidak akan disimpan.
- Terdapat pesan log untuk mencatat bahwa isi file telah dibalik.
- Jika pengguna memasukkan pilihan selain 1 atau 2, program akan menampilkan pesan bahwa pilihan tidak valid.
- Pesan log dicatat untuk mencatat bahwa pemilihan menu tidak valid.
- Fungsi `logData` digunakan untuk mencatat log operasi, baik yang berhasil maupun yang gagal.
```
void folderTulisan (int *result)
{
   char filePath[256] = "data/tulisan/";
       char fileName[256];
       printf("Masukkan nama file yang ingin di-decode: ");
       scanf("%255s", fileName);

       const char *path = "data/tulisan";
       char full_path[512];

       snprintf(full_path, sizeof(full_path), "%s/%s", path, fileName);

       FILE *file = fopen(full_path, "r");
       if (file) {
           fseek(file, 0, SEEK_END);
           long file_size = ftell(file);
           rewind(file);

           char *isi_file = (char *)malloc(file_size + 1);
           fread(isi_file, 1, file_size, file);
           isi_file[file_size] = '\0';

           printf("Isi file sebelum decode:\n%s\n", isi_file);

           char *decoded_content = NULL;

           if (strncmp(fileName, "notes-base64", 11) == 0 || strncmp(fileName, "coba-base64", 10) == 0) {
               decoded_content = base64_decode(isi_file);
           } else if (strncmp(fileName, "enkripsi_rot13", 13) == 0) {
               decoded_content = rot13_decode(isi_file);
           } else if (strncmp(fileName, "new-hex", 7) == 0 || strncmp(fileName, "text-hex", 8) == 0) {
               decoded_content = hex_decode(isi_file);
           } else if (strncmp(fileName, "rev-text", 8) == 0) {
               decoded_content = reverse_text(isi_file);

               logData("SUCCESS", "TULISAN_OPERATION", "File content decoded in Tulisan folder");
           } else {
               printf("Prefix file tidak valid untuk operasi decode yang diimplementasikan.\n");
               free(isi_file);
               fclose(file);
           }

           printf("\nIsi file setelah decode:\n%s\n", decoded_content);

           free(isi_file);
           free(decoded_content);
           fclose(file);
       } else {
           printf("File tidak ditemukan.\n");

           logData("FAILED", "TULISAN_OPERATION", "File content decode failed in Tulisan folder");
       }
       *result = 0;
}
```
- `char filePath[256] = "data/tulisan/";`: Menginisialisasi variabel `filePath` dengan string "data/tulisan/".
- `char fileName[256];`: Deklarasi variabel `fileName` untuk menyimpan nama file yang akan di-decode.
- Menampilkan pesan kepada pengguna untuk memasukkan nama file yang ingin di-decode.
- Menggunakan `scanf` untuk membaca input pengguna dan menyimpannya di dalam variabel `fileName`.
- `const char *path = "data/tulisan";`: Mendeklarasikan konstanta `path` dengan nilai "data/tulisan".
- `char full_path[512];`: Deklarasi variabel `full_path` untuk menyimpan `path` lengkap file.
- Menggunakan `snprintf` untuk menggabungkan path dan `fileName` ke dalam full_path.
- `FILE *file = fopen(full_path, "r");`: Membuka file dengan mode "read" (`"r"`).
- Jika file berhasil dibuka, maka proses selanjutnya dilanjutkan. Jika tidak, program memberikan pesan bahwa file tidak ditemukan dan mencatat log operasi.
- Menggunakan `fseek` untuk mendapatkan ukuran file.
- Mengalokasikan memori untuk menyimpan isi file dalam bentuk string (`isi_file`) dan membacanya menggunakan `fread`.
- Terdapat beberapa kondisi berdasarkan nama file (fileName) untuk menentukan jenis dekode yang akan dilakukan:
    - Jika file memiliki prefix "notes-base64" atau "coba-base64", maka dilakukan base64 decoding.
    - Jika file memiliki prefix "enkripsi_rot13", maka dilakukan rot13 decoding.
    - Jika file memiliki prefix "new-hex" atau "text-hex", maka dilakukan hex decoding.
    - Jika file memiliki prefix "rev-text", maka dilakukan reverse text.
    - Jika tidak ada prefix yang sesuai, program memberikan pesan bahwa prefix file tidak valid dan membebaskan memori yang dialokasikan sebelumnya.
- Jika proses dekode berhasil, isi file sebelum dan setelah dekode ditampilkan menggunakan `printf`.
- Jika operasi dekode berhasil, pesan log dicatat.
- Membebaskan memori yang dialokasikan untuk isi file dan hasil dekode.
- Menutup file setelah selesai operasi.
``` 
void folderData (int *result)
{
   const char *folder_path = "data";
       DIR *dir;
       struct dirent *ent;

       if ((dir = opendir(folder_path)) != NULL) {
           while ((ent = readdir(dir)) != NULL) {
               if (strncmp(ent->d_name, "disable-area", 12) == 0) {
                   lock_folder("data/disable-area");
                   mark_folder_locked();
               }
           }
           closedir(dir);
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           perror("Error opening directory");
       }

       if (access(PASSWORD_FILE, F_OK) == -1) {
           // Jika password belum diatur, set password
           set_folder_password();
       }

       access_folder_disable_area("data/disable-area");

       logData("FAILED", "DISABLE_AREA_OPERATION", "Password file not found");

       *result = 0;
   }
```
- `const char *folder_path = "data";`: Mendeklarasikan konstanta `folder_path` dengan nilai "data".
- `DIR *dir;`: Mendeklarasikan pointer ke struktur direktori.
- `struct dirent *ent;`: Mendeklarasikan pointer ke struktur dirent, yang berisi informasi tentang file/direktori dalam direktori.
- `dir = opendir(folder_path)`: Membuka direktori dengan path yang ditentukan.
- Jika direktori berhasil dibuka, program akan menelusuri setiap file/direktori dalam direktori menggunakan loop `while`.
- Jika nama file/direktori dimulai dengan "disable-area", fungsi `lock_folder` akan dipanggil untuk mengunci folder tersebut dan `mark_folder_locked` untuk menandai bahwa folder tersebut terkunci.
- `access(PASSWORD_FILE, F_OK) == -1`: Mengecek keberadaan file password menggunakan fungsi access.
- Jika file password tidak ditemukan, fungsi `set_folder_password` akan dipanggil untuk mengatur password folder.
- `access_folder_disable_area("data/disable-area")`: Memanggil fungsi untuk mengakses folder "disable-area".
- Pesan log "Password file not found" dicatat jika file password tidak ditemukan.
``` 
void reverseString(char *str, int len) {
   int start = 0;
   int end = len - 1;


   while (start < end) {
       char temp = str[start];
       str[start] = str[end];
       str[end] = temp;
       start++;
       end--;
   }
}
```
- `start` dan `end` diatur untuk menunjukkan ke awal dan akhir string.
- Sebuah loop `while` dijalankan selama `start` kurang dari `end`. Ini berarti pertukaran karakter akan terus dilakukan hingga kita mencapai pertengahan string.
- Karakter pada posisi `start` disimpan dalam variabel sementara `temp`.
- Karakter pada posisi `start` digantikan oleh karakter pada posisi `end`.
- Karakter pada posisi `end` digantikan oleh karakter yang disimpan di variabel `temp`.
- `start` ditingkatkan untuk beralih ke karakter berikutnya di sebelah kanan.
- `end` dikurangkan untuk beralih ke karakter berikutnya di sebelah kiri.
- Loop terus berjalan sampai `start` tidak lagi kurang dari `end`, menandakan bahwa seluruh string telah dibalik.

```
void createDirectory(const char *folderPath) {
   struct stat st = {0};
   if (stat(folderPath, &st) == -1) {
       if (mkdir(folderPath, 0700) != 0) {
           printf("Gagal membuat direktori %s.\n", folderPath);
           exit(EXIT_FAILURE);
       }
   }
}
```
- `struct stat st = {0};` digunakan untuk mendeklarasikan sebuah struktur `stat`. Struktur ini digunakan untuk mendapatkan informasi tentang file atau direktori, termasuk apakah sebuah direktori sudah ada atau tidak.
- `if (stat(folderPath, &st) == -1)` digunakan untuk memeriksa apakah direktori dengan path yang diberikan (`folderPath`) sudah ada atau tidak.
- Jika `stat` mengembalikan -1, itu berarti direktori tidak ditemukan.
- Jika direktori tidak ditemukan, fungsi `mkdir` digunakan untuk membuat direktori dengan path yang diberikan (`folderPath`).
- Argument kedua, `0700`, adalah mode permission yang diberikan kepada direktori yang baru dibuat. Dalam hal ini, `0700` berarti hanya pemilik direktori yang memiliki hak akses penuh.
- Jika `mkdir` mengembalikan nilai selain 0, itu berarti pembuatan direktori gagal.
- Jika pembuatan direktori gagal, pesan kesalahan dicetak ke layar menggunakan `printf`.
- Program keluar dengan status kegagalan (`exit(EXIT_FAILURE)`).

```
void reverseFileName(char *fileName) {
   char *dot = strrchr(fileName, '.');
   int len = (dot == NULL) ? strlen(fileName) : (dot - fileName);
   char extension[10] = "";
   if (dot != NULL) {
       strcpy(extension, dot);
       *dot = '\0';
   }
   reverseString(fileName, len);
   if (dot != NULL) {
       strcat(fileName, extension);


       printf("Membalik nama file: %s\n", fileName);
   }
}
```
- `char *dot = strrchr(fileName, '.');` digunakan untuk mencari posisi titik terakhir dalam nama file. Fungsi `strrchr` mencari karakter '.' dari belakang ke depan.
- `int len = (dot == NULL) ? strlen(fileName) : (dot - fileName);` menghitung panjang nama file tanpa ekstensi. Jika titik tidak ditemukan, panjang adalah seluruh panjang nama file; jika titik ditemukan, panjang adalah jarak antara awal dan titik.
- Jika titik (ekstensi) ditemukan, ekstensi disalin ke dalam array `extension` dan posisi titik diubah menjadi null (`*dot = '\0'`), sehingga hanya nama file yang akan dibalik.
- Fungsi `reverseString` dipanggil untuk membalik nama file tanpa ekstensi.
- Jika titik (ekstensi) ditemukan sebelumnya, ekstensi ditambahkan kembali ke nama file yang sudah dibalik menggunakan `strcat`.
```
void reverseIsiFile(const char *fileName) {
   FILE *file = fopen(fileName, "rb");
   if (file == NULL) {
       printf("File tidak ditemukan atau tidak dapat dibuka.\n");
       return;
   }


   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);


   char *isiFile = (char *)malloc(fileSize + 1);
   fread(isiFile, fileSize, 1, file);
   isiFile[fileSize] = '\0';
   fclose(file);


   reverseString(isiFile, fileSize);
   FILE *reverseFile = fopen(fileName, "wb");
   if (reverseFile == NULL) {
       printf("File tidak dapat dibuka untuk penulisan.\n");
       free(isiFile);
       return;
   }
   fwrite(isiFile, fileSize, 1, reverseFile);
   fclose(reverseFile);
   free(isiFile);
   printf("Isi file %s telah dibalikkan.\n", fileName);
}
```
- `FILE *file = fopen(fileName, "rb");` membuka file dalam mode binary ("rb") untuk dibaca.
- Jika file tidak dapat dibuka, cetak pesan kesalahan dan keluar dari fungsi.
- `fseek` dan `ftell` digunakan untuk mengukur ukuran file.
- Memori dinamis dialokasikan untuk menyimpan isi file menggunakan `malloc`.
- Fungsi `fread` digunakan untuk membaca seluruh isi file ke dalam memori.
- Setelah isi file dibaca, file ditutup.
- Fungsi `reverseString` dipanggil untuk membalikkan isi file yang sudah dibaca.
- `FILE *reverseFile = fopen(fileName, "wb");` membuka file dalam mode binary ("wb") untuk penulisan.
- Jika file tidak dapat dibuka, cetak pesan kesalahan, bebaskan memori, dan keluar dari fungsi.
- Fungsi `fwrite` digunakan untuk menulis isi file yang sudah dibalikkan ke file yang baru dibuka untuk penulisan.
- Setelah penulisan selesai, file yang baru dibuat ditutup.
- Memori yang dialokasikan untuk menyimpan isi file dibebaskan.
- Cetak pesan ke layar bahwa isi file telah berhasil dibalikkan.
```
char* base64_decode(const char* input) {
   BIO *bio, *b64, *bio_out;


 int decoded_size = 0;


   int input_length = strlen(input);
   int max_decoded_length = (input_length * 3) / 4; // Estimasi panjang maksimal setelah decode
   char* buffer = (char*)malloc(max_decoded_length);


   b64 = BIO_new(BIO_f_base64());
   bio = BIO_new_mem_buf((void*)input, -1);
   bio = BIO_push(b64, bio);


   bio_out = BIO_new(BIO_s_mem());
   BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);


   decoded_size = BIO_read(bio, buffer, input_length);
   buffer[decoded_size] = '\0';


   BIO_free_all(bio);


   return buffer;
}
```
- `BIO *bio, *b64, *bio_out;: Deklarasi objek BIO (I/O abstraksi)` yang digunakan untuk membaca, mendecode, dan menyimpan hasil decode.
- `int decoded_size = 0;`: Variabel untuk menyimpan ukuran hasil decode.
- `int input_length = strlen(input);`: Mengukur panjang string input.
- `int max_decoded_length = (input_length * 3) / 4;`: Estimasi panjang maksimal buffer hasil decode. Dalam base64, setiap 4 karakter ASCII akan diubah menjadi 3 byte data biner.
- `char* buffer = (char*)malloc(max_decoded_length);`: Membuat buffer untuk menyimpan hasil decode.
- `b64 = BIO_new(BIO_f_base64());`: Membuat objek BIO untuk base64.
- `bio = BIO_new_mem_buf((void*)input, -1);`: Membuat objek BIO dari string input.
- `bio = BIO_push(b64, bio);`: Menggabungkan BIO untuk base64 dengan BIO dari string input.
- `bio_out = BIO_new(BIO_s_mem())`;: Membuat objek BIO untuk menyimpan hasil decode.
- `BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);`: Mengatur flag untuk BIO base64 agar tidak menghasilkan newline.
- `decoded_size = BIO_read(bio, buffer, input_length);`: Membaca dan mendecode string input ke dalam buffer.
- `buffer[decoded_size] = '\0';`: Menambahkan null terminator pada akhir buffer.
- `BIO_free_all(bio);`: Membebaskan semua objek BIO.
- `return buffer;`: Mengembalikan buffer hasil decode.
```
char* rot13_decode(const char* input) {
   char* result = strdup(input);
   if (result == NULL) {
       return NULL;
   }
   for (int i = 0; result[i] != '\0'; i++) {
       if (isalpha(result[i])) {
           char base = islower(result[i]) ? 'a' : 'A';
           result[i] = (result[i] - base + 13) % 26 + base;
       }
   }
   return result;
}
```
- `char* result = strdup(input);`: Mengalokasikan memori dan menginisialisasi result dengan salinan string input. Fungsi strdup digunakan untuk membuat salinan string yang baru, dan alokasi memori dilakukan secara dinamis.
- `if (result == NULL) { return NULL; }:` Memeriksa apakah alokasi memori berhasil. Jika tidak, fungsi mengembalikan NULL untuk menunjukkan kegagalan alokasi.
- `for (int i = 0; result[i] != '\0'; i++) { ... }:` Melakukan iterasi melalui setiap karakter dalam string hasil dekripsi.
- `if (isalpha(result[i])) { ... }:` Memeriksa apakah karakter saat ini adalah huruf.
- `char base = islower(result[i]) ? 'a' : 'A';`: Menentukan huruf dasar ('a' atau 'A') berdasarkan huruf kecil atau besar.
- `result[i] = (result[i] - base + 13) % 26 + base;`: Melakukan perhitungan untuk mendekripsi karakter dengan algoritma ROT13. Karakter diubah sesuai dengan pergeseran 13 tempat dalam alfabet.
- `return result;`: Mengembalikan pointer ke string hasil dekripsi. Itu adalah tanggung jawab pengguna untuk memastikan bahwa memori ini dibebaskan setelah digunakan dengan menggunakan free().
```
char* hex_decode(const char* input) {
   int input_length = strlen(input);
   if (input_length % 2 != 0) {
       return NULL;
   }
   int output_length = input_length / 2;
   char* buffer = (char*)malloc(output_length + 1);
   if (buffer == NULL) {
       return NULL;
   }
   for (int i = 0; i < output_length; i++) {
       int high = input[i * 2];
       int low = input[i * 2 + 1];
       if (isxdigit(high) && isxdigit(low)) {
           high = isdigit(high) ? high - '0' : (toupper(high) - 'A') + 10;
           low = isdigit(low) ? low - '0' : (toupper(low) - 'A') + 10;
           buffer[i] = (high << 4) | low;
       } else {
           free(buffer);
           return NULL;
       }
   }
   buffer[output_length] = '\0';
   return buffer;
}
```
- `int input_length = strlen(input);`: Mendapatkan panjang string input.
- `if (input_length % 2 != 0) { return NULL; }`: Memeriksa apakah panjang input adalah bilangan genap. Panjang harus genap karena setiap dua karakter heksadesimal akan diubah menjadi satu byte data.
- `int output_length = input_length / 2;`: Menghitung panjang output. Setiap dua karakter heksadesimal akan diubah menjadi satu byte data biner.
- `char* buffer = (char*)malloc(output_length + 1);`: Mengalokasikan memori untuk buffer hasil dekripsi, ditambah satu untuk null terminator.
- `if (buffer == NULL) { return NULL; }`: Memeriksa apakah alokasi memori berhasil.
- `for (int i = 0; i < output_length; i++) { ... }`: Melakukan iterasi melalui setiap pasangan karakter heksadesimal.
- `int high = input[i * 2]; int low = input[i * 2 + 1];`: Mendapatkan karakter tinggi (high) dan rendah (low) dari pasangan karakter heksadesimal.
- `if (isxdigit(high) && isxdigit(low)) { ... }`: Memeriksa apakah karakter tinggi dan rendah adalah karakter heksadesimal yang valid.
- `high = isdigit(high) ? high - '0' : (toupper(high) - 'A') + 10; low = isdigit(low) ? low - '0' : (toupper(low) - 'A') + 10;`: Konversi karakter heksadesimal ke nilai numerik.
- `buffer[i] = (high << 4) | low;`: Menggabungkan nilai tinggi dan rendah untuk membentuk satu byte.
- `else { free(buffer); return NULL; }`: Jika karakter tidak valid, bebaskan memori dan kembalikan NULL.
- `buffer[output_length] = '\0';`: Menambahkan null terminator pada akhir buffer.
- `return buffer;`: Mengembalikan pointer ke buffer hasil dekripsi.
```
char* reverse_text(const char* input) {
   int length = strlen(input);
   char* reversed = (char*)malloc(length + 1);
   if (reversed == NULL) {
       return NULL;
   }
   for (int i = 0; i < length; i++) {
       reversed[i] = input[length - i - 1];
   }
   reversed[length] = '\0';
   return reversed;
}
```
- `int length = strlen(input);`: Mendapatkan panjang (jumlah karakter) dari string input menggunakan fungsi `strlen`.
- `char* reversed = (char*)malloc(length + 1);`: Mengalokasikan memori dinamis untuk menyimpan string yang akan dihasilkan setelah dibalik. Panjang string +1 digunakan untuk menyertakan karakter null-terminator ('\0') di akhir string.
- `if (reversed == NULL) { return NULL; }`: Memeriksa apakah alokasi memori berhasil. Jika alokasi gagal, fungsi mengembalikan nilai `NULL`.
- `for (int i = 0; i < length; i++) { reversed[i] = input[length - i - 1]; }`: Melakukan iterasi melalui setiap karakter dalam string input dan menempatkan karakter tersebut ke posisi yang sesuai dalam string yang baru (`reversed`). Proses ini dilakukan dengan memulai dari karakter terakhir dari string input dan memindahkannya ke posisi pertama dalam string yang dibalik.
- `reversed[length] = '\0';`: Menambahkan karakter null-terminator di akhir string yang dibalik untuk memastikan bahwa string tersebut ditutup dengan baik.
- `return reversed;`: Mengembalikan pointer ke string yang telah dibalik.
```
void lock_folder(const char *folder_path) {
   const char *path = "data/disable-area";
   // Mengecek apakah folder sudah ada atau belum
   struct stat st = {0};
   if (stat(folder_path, &st) == -1) {
       // Jika folder belum ada, maka buat folder
       if (mkdir(folder_path, 0700) == -1) {
           perror("Error creating folder");
  
       } else {
           printf("Folder created successfully: %s\n", folder_path);
       }
   } else {
       printf("Folder already exists: %s\n", folder_path);
   }
   // Mengunci akses folder
   if (chmod(folder_path, 0500) == -1) {
       perror("Error changing folder permissions");


   } else {
       printf("Folder locked successfully: %s\n", folder_path);
   }
}
```
- `struct stat st = {0};`: Membuat struktur `stat` untuk menyimpan informasi status dari folder yang akan diperiksa.
- `if (stat(folder_path, &st) == -1) { ... }`: Memeriksa apakah folder sudah ada atau belum dengan menggunakan fungsi `stat`. Jika folder belum ada, maka program akan membuat folder tersebut.
- `if (mkdir(folder_path, 0700) == -1) { perror("Error creating folder"); ... }`: Jika folder belum ada, maka program akan mencoba membuat folder menggunakan fungsi `mkdir` dengan mode akses 0700 (hanya pemilik folder yang memiliki hak akses). Jika pembuatan folder gagal, program akan menampilkan pesan kesalahan menggunakan `perror`.
- printf("Folder created successfully: %s\n", folder_path);: Jika folder berhasil dibuat, program akan menampilkan pesan sukses`
- printf("Folder already exists: %s\n", folder_path);: Jika folder sudah ada, program akan menampilkan pesan bahwa folder tersebut sudah ada.
- `if (chmod(folder_path, 0500) == -1) { perror("Error changing folder permissions"); ... }`: Menggunakan fungsi chmod untuk mengganti izin folder. Mode 0500 berarti hanya pemilik folder yang memiliki hak akses (read, write, dan execute), sedangkan grup dan lainnya tidak memiliki hak akses.
- printf("Folder locked successfully: %s\n", folder_path);: Jika penguncian folder berhasil, program akan menampilkan pesan sukses.
```
void mark_folder_locked() {
   FILE *statusFile = fopen("kunci_status.txt", "w");
   if (statusFile != NULL) {
       fprintf(statusFile, "Folder ini telah dikunci.\n");
       fclose(statusFile);
   } else {
       perror("Error opening status file");
       exit(EXIT_FAILURE);
   }
}
```
- `FILE *statusFile = fopen("kunci_status.txt", "w");` Membuka file dengan nama "kunci_status.txt" dalam mode penulisan ("w"). Fungsi `fopen` mengembalikan pointer ke struktur FILE yang digunakan untuk operasi file berikutnya.
- `if (statusFile != NULL)` Memeriksa apakah pembukaan file berhasil. Jika `fopen` berhasil, maka pointer file tidak akan bernilai NULL.
- `fprintf(statusFile, "Folder ini telah dikunci.\n");` Menggunakan fungsi fprintf untuk menuliskan string "Folder ini telah dikunci." ke dalam file yang telah dibuka.
- `fclose(statusFile);` Menutup file setelah selesai menulis. Ini adalah tindakan yang baik untuk mencegah kebocoran sumber daya dan memastikan bahwa perubahan telah disimpan.
- `} else {` Bagian ini menangani situasi di mana fopen gagal membuka file.
- `perror("Error opening status file");` Mencetak pesan kesalahan spesifik ke dalam file stderr yang menggambarkan alasan kegagalan membuka file.
- `exit(EXIT_FAILURE);` Mengakhiri program dengan status keluar EXIT_FAILURE. Ini menunjukkan bahwa program tidak dapat menyelesaikan operasi yang diinginkan dan mengalami kegagalan.
```
void set_folder_password() {
   char password[50];
   printf("Buat password untuk folder disable-area: ");
   fgets(password, sizeof(password), stdin);


   FILE *file = fopen(PASSWORD_FILE, "w");
   if (file == NULL) {
       perror("Error creating password file");
       exit(EXIT_FAILURE);
   }


   fprintf(file, "%s", password);
   fclose(file);
}
```
- `char password[50];` Membuat sebuah array karakter dengan nama password yang dapat menampung hingga 50 karakter.
- `printf("Buat password untuk folder disable-area: ");` Mencetak pesan ke layar untuk meminta pengguna memasukkan password untuk folder dengan nama "disable-area".
- `fgets(password, sizeof(password), stdin);` Menggunakan fungsi `fgets` untuk membaca password dari input pengguna dan menyimpannya dalam array `password`. `sizeof(password)` menentukan panjang maksimal string yang dapat dibaca, sehingga memastikan bahwa tidak lebih dari 50 karakter akan dibaca.
- `FILE *file = fopen(PASSWORD_FILE, "w");` Membuka file dengan nama yang disimpan dalam konstanta `PASSWORD_FILE` dalam mode penulisan ("w"). Konstanta `PASSWORD_FILE` tidak ditampilkan dalam code yang diberikan, jadi kita perlu mengetahui nilai sebenarnya dari konstanta tersebut untuk memahami kode dengan lebih baik.
- `if (file == NULL)` Memeriksa apakah pembukaan file berhasil. Jika `fopen` gagal, maka pointer file akan bernilai NULL.
- `perror("Error creating password file");` Mencetak pesan kesalahan spesifik ke dalam file stderr yang menggambarkan alasan kegagalan membuka file.
- `exit(EXIT_FAILURE);` Mengakhiri program dengan status keluar EXIT_FAILURE. Ini menunjukkan bahwa program tidak dapat menyelesaikan operasi yang diinginkan dan mengalami kegagalan.
- `fprintf(file, "%s", password);` Menggunakan fprintf untuk menulis password yang telah diinputkan oleh pengguna ke dalam file.
- `fclose(file);`
Menutup file setelah selesai menulis. Ini adalah tindakan yang baik untuk mencegah kebocoran sumber daya dan memastikan bahwa perubahan telah disimpan.
```
int authenticate_user() {
   char entered_password[50];

   for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt) {
       printf("Masukkan password untuk mengakses folder disable-area (Percobaan %d/%d): ", attempt, MAX_ATTEMPTS);
       fgets(entered_password, sizeof(entered_password), stdin);

       FILE *file = fopen(PASSWORD_FILE, "r");
       if (file == NULL) {
           perror("Error reading password file");
           exit(EXIT_FAILURE);
       }

       char correct_password[50];
       fgets(correct_password, sizeof(correct_password), file);
       fclose(file);

       if (strcmp(entered_password, correct_password) == 0) {
           return 1; // Password benar, izin diberikan
       } else {
           printf("Password salah. ");
       }
   }

   return 0; // Melebihi batas percobaan, izin ditolak
}
```
- `char entered_password[50];` Membuat sebuah array karakter dengan nama `entered_password` yang dapat menampung hingga 50 karakter untuk menyimpan password yang diinput oleh pengguna.
- `for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt)` {
Memulai loop untuk memberikan pengguna beberapa kesempatan (maksimal sebanyak `MAX_ATTEMPTS`) untuk memasukkan password yang benar.
- `printf("Masukkan password untuk mengakses folder disable-area (Percobaan %d/%d): ", attempt, MAX_ATTEMPTS);` Mencetak pesan ke layar yang meminta pengguna untuk memasukkan password dengan menampilkan nomor percobaan saat ini dan total percobaan yang diperbolehkan.
- `fgets(entered_password, sizeof(entered_password), stdin);` Menggunakan fungsi `fgets` untuk membaca password yang diinputkan oleh pengguna dari stdin (input standar) dan menyimpannya dalam array `entered_password`.
- `FILE *file = fopen(PASSWORD_FILE, "r");` Membuka file dengan nama yang disimpan dalam konstanta `PASSWORD_FILE` dalam mode membaca ("r"). Konstanta `PASSWORD_FILE` tidak ditampilkan dalam code yang diberikan, jadi kita perlu mengetahui nilai sebenarnya dari konstanta tersebut untuk memahami kode dengan lebih baik.
- `if (file == NULL)` Memeriksa apakah pembukaan file berhasil. Jika fopen gagal, maka pointer file akan bernilai NULL.
- `perror("Error reading password file");` Mencetak pesan kesalahan spesifik ke dalam file stderr yang menggambarkan alasan kegagalan membaca file.
- `char correct_password[50];` Membuat array karakter dengan nama `correct_password` untuk menyimpan password yang benar yang dibaca dari file.
- `fgets(correct_password, sizeof(correct_password), file);`Menggunakan fungsi `fgets` untuk membaca password yang benar dari file dan menyimpannya dalam array `correct_password.`
- `fclose(file);` Menutup file setelah selesai membaca. Ini adalah tindakan yang baik untuk mencegah kebocoran sumber daya.
- `if (strcmp(entered_password, correct_password) == 0) {` Membandingkan password yang diinputkan oleh pengguna dengan password yang benar yang dibaca dari file menggunakan fungsi `strcmp`. Jika kedua password sama, fungsi mengembalikan nilai 1, menunjukkan bahwa autentikasi berhasil.
-` return 1; // Password benar, izin diberikan`
Jika password benar, fungsi mengembalikan nilai 1.
- `} else { printf("Password salah. ");` Jika password tidak sama, lanjutkan dengan pesan bahwa password salah.
- `return 0; // Melebihi batas percobaan, izin ditolak` Jika program mencapai titik ini, berarti pengguna telah melebihi batas percobaan yang diperbolehkan, dan fungsi mengembalikan nilai 0 untuk menunjukkan bahwa autentikasi ditolak.
```
int access_folder_disable_area(const char *folder_path) {
   DIR *dir;
   struct dirent *ent;

   if ((dir = opendir(folder_path)) != NULL) {
       int success = 0;

       while ((ent = readdir(dir)) != NULL) {
           if (strncmp(ent->d_name, "disable-area", 12) == 0) {
               success = 1;
               break;
           }
       }

       closedir(dir);

       if (success) {
           // Folder ditemukan, lakukan operasi yang diperlukan
           lock_folder(folder_path);
           mark_folder_locked();
           logData("SUCCESS", "DISABLE_AREA_OPERATION", "Accessed Disable Area folder");
       } else {
           // Folder tidak ditemukan
           logData("FAILED", "DISABLE_AREA_OPERATION", "Folder not found");
       }

       return success;  // Mengemreverse status keberhasilan
   } else {
       // Terjadi kesalahan saat membuka direktori
       perror("Error opening directory");
       logData("FAILED", "DISABLE_AREA_OPERATION", "Error opening directory");

       return -1;  // Mengemreverse status kesalahan
   }
}
```
- Membuka direktori dengan path yang diberikan menggunakan fungsi opendir.
- Jika pembukaan direktori berhasil, maka iterasi melalui setiap entri dalam direktori untuk mencari subfolder dengan nama "disable-area".
- Jika subfolder ditemukan, fungsi akan mengunci folder, menandai bahwa folder tersebut dikunci, dan mencatat operasi yang berhasil dalam log.
- Jika subfolder tidak ditemukan, fungsi mencatat kegagalan dalam log.
- Fungsi mengembalikan nilai yang mengindikasikan keberhasilan atau kesalahan operasi:
    - Jika subfolder ditemukan, mengembalikan 1.
    - Jika subfolder tidak ditemukan, mengembalikan 0.
    - Jika terjadi kesalahan saat membuka direktori, mengembalikan -1 dan mencatat kesalahan dalam log.
```
void logData(const char *status, const char *tag, const char *information) {
   FILE *log = fopen(LOG_FILE, "a");
   if (log != NULL) {
       time_t rawtime;
       struct tm *timeinfo;
       time(&rawtime);
       timeinfo = localtime(&rawtime);


       char timestamp[MAX_LOG_LENGTH];
       strftime(timestamp, MAX_LOG_LENGTH, "[%d/%m/%Y-%H:%M:%S]", timeinfo);


       fprintf(log, "[%s]::%s::[%s]::[%s]\n", status, timestamp, tag, information);
       fclose(log);
   } else {
       printf("Tidak dapat membuka file log untuk penulisan.\n");
   }
}
```
- `FILE *log = fopen(LOG_FILE, "a");` Membuka file log dengan nama yang disimpan dalam konstanta LOG_FILE dalam mode append ("a"). Jika pembukaan file berhasil, pointer file (log) akan ditetapkan; jika tidak, pointer file akan NULL.
- `if (log != NULL) {` Memeriksa apakah pembukaan file log berhasil.
- `time_t rawtime;` Membuat variabel rawtime untuk menyimpan waktu dalam format timestamp.
- `struct tm *timeinfo;` Membuat pointer ke struktur tm untuk menyimpan informasi waktu lokal.
- `time(&rawtime);` Mengisi variabel rawtime dengan waktu saat ini.
``` 
static int xmp_fullpath(char *fullpath, const char *path) {
   strcpy(fullpath, base_path);
   strncat(fullpath, path, PATH_MAX);
   return 0;
}
```
- `strcpy(fullpath, base_path);` Menggunakan fungsi `strcpy` untuk menyalin isi dari `base_path` ke `fullpath`. Ini menetapkan base path sebagai awal dari path lengkap.
- `strncat(fullpath, path, PATH_MAX);` Menggunakan fungsi `strncat` untuk menambahkan isi dari `path` ke `fullpath`. `PATH_MAX` menunjukkan panjang maksimum string yang dapat ditambahkan, sehingga ini membantu mencegah buffer overflow.
- `return 0;` Mengembalikan nilai 0 untuk menandakan bahwa fungsi telah selesai dengan sukses.
```
static int xmp_getattr(const char *path, struct stat *stbuf) {
   int res;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   res = lstat(fullpath, stbuf);

   if (res == -1)
       return -errno;

   return 0;
}
```
- `int res;` Mendeklarasikan variabel `res` bertipe integer. Variabel ini akan digunakan untuk menyimpan hasil dari pemanggilan fungsi `lstat`.
- `char fullpath[PATH_MAX];` Mendeklarasikan array karakter `fullpath` dengan ukuran maksimum sepanjang `PATH_MAX`. Array ini akan digunakan untuk menyimpan path lengkap dari file atau direktori.
- `xmp_fullpath(fullpath, path);` Memanggil fungsi `xmp_fullpath` untuk membangun path lengkap dengan menggabungkan `base_path` dan `path`. Hasilnya disimpan dalam array `fullpath`.
- `res = lstat(fullpath, stbuf);` Memanggil fungsi `lstat` untuk mendapatkan atribut dari file atau direktori dengan path lengkap yang telah dibangun. Hasilnya disimpan dalam variabel `res`.
- `if (res == -1)` Memeriksa apakah pemanggilan fungsi `lstat` menghasilkan kesalahan.
- `return -errno;`
Jika terjadi kesalahan, fungsi mengembalikan nilai negatif dari variabel `errno`. Ini memberikan informasi tentang jenis kesalahan yang terjadi.
- `return 0;` Jika tidak ada kesalahan, fungsi mengembalikan nilai 0 untuk menandakan bahwa operasi berhasil.
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
   DIR *dp;
   struct dirent *de;
   (void)offset;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   dp = opendir(fullpath);

   if (dp == NULL)
       return -errno;

   while ((de = readdir(dp)) != NULL) {
       struct stat st;

       memset(&st, 0, sizeof(st));

       st.st_ino = de->d_ino;
       st.st_mode = de->d_type << 12;

       if (filler(buf, de->d_name, &st, 0))
           break;
   }
   closedir(dp);
   return 0;
}
```
- `DIR *dp;` Mendeklarasikan pointer `dp` dari tipe `DIR`, yang akan digunakan untuk merepresentasikan direktori.
- `struct dirent *de;` Mendeklarasikan pointer `de` dari tipe `struct dirent`, yang akan digunakan untuk merepresentasikan entri direktori.
- `(void)offset;` Menggunakan `(void)` untuk menunjukkan bahwa parameter `offset` tidak digunakan dalam fungsi ini. Ini dilakukan untuk menghindari munculnya peringatan (warning) kompilasi terkait parameter yang tidak digunakan.
- `(void)fi;` Menggunakan `(void)` untuk menunjukkan bahwa parameter `fi` (struct `fuse_file_info`) tidak digunakan dalam fungsi ini. Sama seperti sebelumnya, ini dilakukan untuk menghindari munculnya peringatan kompilasi terkait parameter yang tidak digunakan.
- `char fullpath[PATH_MAX];` Mendeklarasikan array karakter `fullpath` dengan ukuran maksimum sepanjang `PATH_MAX`. Array ini akan digunakan untuk menyimpan path lengkap dari direktori yang akan dibaca.
- `xmp_fullpath(fullpath, path);` Memanggil fungsi `xmp_fullpath` untuk membangun path lengkap dengan menggabungkan `base_path` dan `path`. Hasilnya disimpan dalam array `fullpath`.
- `dp = opendir(fullpath);` Membuka direktori yang diidentifikasi oleh path lengkap yang telah dibangun.
- `if (dp == NULL)` Memeriksa apakah pembukaan direktori berhasil.
- `return -errno;` Jika terjadi kesalahan, fungsi mengembalikan nilai negatif dari variabel `errno`. Ini memberikan informasi tentang jenis kesalahan yang terjadi.
- `while ((de = readdir(dp)) != NULL) {` Looping melalui entri-entri direktori.
- `struct stat st;` Mendeklarasikan variabel `st` dari tipe `struct stat`, yang akan digunakan untuk menyimpan informasi atribut dari setiap entri direktori.
- `memset(&st, 0, sizeof(st));` Menginisialisasi `st` dengan nilai nol menggunakan fungsi `memset`.
- `st.st_ino = de->d_ino;` Menetapkan nilai inode dari entri direktori ke dalam `st`.
- `st.st_mode = de->d_type << 12;` Menetapkan nilai mode dari entri direktori ke dalam `st`. Bitwise shift digunakan untuk memindahkan nilai `d_type` ke posisi yang benar dalam nilai mode.
- `if (filler(buf, de->d_name, &st, 0))` Memanggil fungsi `filler` untuk mengisi `buf` dengan informasi entri direktori. Jika fungsi `filler` mengembalikan nilai non-nol, loop akan dihentikan.
- `break;` Keluar dari loop jika fungsi `filler` mengembalikan nilai non-nol.
- `closedir(dp);` Menutup direktori setelah selesai membacanya.
- `return 0;` Mengembalikan nilai 0 untuk menandakan bahwa operasi berhasil.
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
   int fd;
   int res;
   (void)fi;
   char fullpath[PATH_MAX];

   xmp_fullpath(fullpath, path);

   fd = open(fullpath, O_RDONLY);

   if (fd == -1)
       return -errno;

   res = pread(fd, buf, size, offset);

   if (res == -1)
       res = -errno;

   close(fd);

   return res;
}
```
- `int fd;` Mendeklarasikan variabel `fd` bertipe integer, yang akan digunakan sebagai file descriptor.
- `int res;` Mendeklarasikan variabel `res` bertipe integer, yang akan digunakan untuk menyimpan hasil dari pemanggilan fungsi `pread.`
- `(void)fi;` Menggunakan `(void)` untuk menunjukkan bahwa parameter `fi` (struct `fuse_file_info`) tidak digunakan dalam fungsi ini. Ini dilakukan untuk menghindari munculnya peringatan kompilasi terkait parameter yang tidak digunakan.
- `char fullpath[PATH_MAX];` Mendeklarasikan array karakter `fullpath` dengan ukuran maksimum sepanjang `PATH_MAX`. Array ini akan digunakan untuk menyimpan path lengkap dari file yang akan dibaca.
- `xmp_fullpath(fullpath, path);` Memanggil fungsi `xmp_fullpath` untuk membangun path lengkap dengan menggabungkan `base_path` dan `path`. Hasilnya disimpan dalam array `fullpath`.
- `fd = open(fullpath, O_RDONLY);` Membuka file dengan path lengkap yang telah dibangun dalam mode hanya baca (`O_RDONLY`). Hasilnya disimpan dalam variabel fd.
- `if (fd == -1)` Memeriksa apakah pembukaan file berhasil.
- `return -errno;` Jika terjadi kesalahan, fungsi mengembalikan nilai negatif dari variabel `errno`. Ini memberikan informasi tentang jenis kesalahan yang terjadi.
- `res = pread(fd, buf, size, offset);` Menggunakan fungsi `pread` untuk membaca data dari file ke dalam buffer `buf`. `size` menunjukkan ukuran data yang akan dibaca, dan `offset` menunjukkan posisi awal pembacaan dalam file. Hasilnya disimpan dalam variabel `res`.
- `if (res == -1)` Memeriksa apakah fungsi pread menghasilkan kesalahan.
- `res = -errno;` Jika terjadi kesalahan, nilai negatif dari variabel `errno` disimpan dalam variabel `res`.
- `close(fd);` Menutup file setelah selesai membacanya.
- `return res;` Mengembalikan nilai `res`, yang berisi hasil dari operasi membaca. Jika operasi berjalan dengan sukses, nilai positif akan dihasilkan; jika tidak, nilai negatif dari `errno` akan dikembalikan untuk memberikan informasi tentang jenis kesalahan yang terjadi.
```
static struct fuse_operations xmp_oper = {
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,
};
```
- `xmp_getattr`: Mengacu pada fungsi `xmp_getattr` yang dijelaskan sebelumnya. Digunakan untuk mendapatkan atribut (metadata) dari file atau direktori.
- `xmp_readdir`: Mengacu pada fungsi `xmp_readdir` yang dijelaskan sebelumnya. Digunakan untuk membaca isi dari suatu direktori.
- `xmp_read`: Mengacu pada fungsi `xmp_read` yang dijelaskan sebelumnya. Digunakan untuk membaca isi dari suatu file.
```
void run_fuse() {
   char *fuse_argv[] = {"hell", "/home/angel/sisop/Sisop_Modul_4/mount", "-f", NULL}; // Adjust the mount point accordingly
   int fuse_argc = sizeof(fuse_argv) / sizeof(fuse_argv[0]) - 1;

   fuse_main(fuse_argc, fuse_argv, &xmp_oper, NULL);
}
```
- `char *fuse_argv[] = {"hell", "/home/angel/sisop/Sisop_Modul_4/mount", "-f", NULL};` Mendefinisikan array `fuse_argv` yang berisi argumen-argumen yang akan digunakan ketika menjalankan FUSE. Argumen tersebut antara lain:
    - `"hell"`: Nama program yang dijalankan (sebagai placeholder).
    - `"/home/angel/sisop/Sisop_Modul_4/mount"`: Path tempat untuk me-mount FUSE filesystem. Harap disesuaikan sesuai kebutuhan.
    - `"-f"`: Argumen untuk menjalankan FUSE dalam mode foreground.
NULL: Menandakan akhir dari array argumen.
- `int fuse_argc = sizeof(fuse_argv) / sizeof(fuse_argv[0]) - 1;` Menghitung jumlah argumen dengan menghitung ukuran array dan membaginya dengan ukuran satu elemen array. Dikurangi 1 karena elemen terakhir adalah NULL.
- `fuse_main(fuse_argc, fuse_argv, &xmp_oper, NULL);` Memanggil fungsi `fuse_main` untuk menjalankan sistem berkas FUSE. Parameter `fuse_argc` dan `fuse_argv` menyediakan argumen yang akan digunakan. `&xmp_oper` adalah pointer ke struktur `struct fuse_operations` yang telah didefinisikan sebelumnya. Parameter terakhir, NULL, menyediakan data tambahan yang tidak digunakan dalam contoh ini.
```
int main(int argc, char *argv[]) {
   umask(0);

   pid_t fuse_pid;

   // Create a process to run the FUSE file system
   if ((fuse_pid = fork()) == -1) {
       fprintf(stderr, "Error creating FUSE process\n");
       return 1;
   }
   if (fuse_pid == 0) {
       // This is the child process (FUSE)
       run_fuse();
       exit(0);
   }
   int folderChoice;
   do {
       printf("Pilih folder:\n");
       printf("1. Gallery\n");
       printf("2. Sisop\n");
       printf("3. Tulisan\n");
       printf("4. Data\n");
       printf("Pilihan Anda: ");
       scanf("%d", &folderChoice);

       // Clear the input buffer
       int c;
       while ((c = getchar()) != '\n' && c != EOF);

       switch (folderChoice) {
       case 1:
           folderGallery("data/gallery", &result);
           break;
       case 2:
           folderSisop("data/sisop", &result);
           break;
       case 3:
           folderTulisan(&result);
           break;
       case 4:
           folderData(&result);
           break;
       default:
           printf("Pilihan tidak valid. Coba lagi.\n");
   }
} while (1);

   int status;
   waitpid(fuse_pid, &status, 0);

   return 0;
}
```
- `umask(0);` Menetapkan `umask` ke 0, yang memastikan bahwa hak akses file yang baru dibuat tidak akan diubah oleh mask yang diberlakukan.
- `pid_t fuse_pid;` Mendeklarasikan variabel untuk menyimpan PID (Process ID) dari proses FUSE yang akan dibuat.
- `if ((fuse_pid = fork()) == -1)` Mencoba membuat proses anak dengan fungsi `fork()`. Jika gagal, program mencetak pesan kesalahan dan keluar.
- `if (fuse_pid == 0)` Bagian ini hanya dijalankan oleh proses anak (proses FUSE). Memanggil fungsi `run_fuse()` untuk menjalankan FUSE dan kemudian keluar dari proses anak.
- `int folderChoice;` Mendeklarasikan variabel untuk menyimpan pilihan folder yang akan diproses.
- `do { ... } while (1);` Looping tak terbatas untuk meminta input pengguna dan memproses folder sesuai pilihan.
- `printf("Pilih folder:\n");` Menampilkan menu pilihan folder kepada pengguna.
- `scanf("%d", &folderChoice);` Menerima input pilihan dari pengguna.
- while ((c = getchar()) != '\n' && c != EOF); Membersihkan buffer input agar tidak ada masukan yang tersisa.
- `switch (folderChoice) { ... }` Menggunakan switch untuk memproses pilihan folder yang dimasukkan oleh pengguna.
- `int status;` Mendeklarasikan variabel untuk menyimpan status keluar dari proses FUSE.
- `waitpid(fuse_pid, &status, 0);` Menunggu proses FUSE untuk selesai dan menyimpan status keluarannya.
- `return 0;` Mengakhiri program dengan mengembalikan nilai 0.

# Output
Folder Gallery

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/a02b8bea-be90-4404-b1ec-c6b7b23a8162)

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/5cfeef78-d585-4056-b6e0-ac9b7825344c)

Folder Sisop

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/70777fc2-6230-4fca-96cc-8ad0e355541e)

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/8defc5fb-1d4a-46eb-a3f4-0279667b0531)

Folder Tulisan

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/b933b4ba-41e9-4984-a2bb-d8ffaf54ad41)

Folder Disable-area

![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/22b0ad10-0eac-45e1-a058-75dff27e9ab2)

Folder mount-point
![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/76332485-78f5-4f63-bbe4-5758dd048f62)

log
![image](https://github.com/Angel0010/bms-demo-angel/assets/131789727/fe923f8d-255e-44b2-9f39-dd14ac5f7303)



# Soal 2
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari
Membuat file open-password.c untuk membaca file zip-pass.txt
Melakukan proses dekripsi base64 terhadap file tersebut
Lalu unzip home.zip menggunakan password hasil dekripsi
Membuat file semangat.c
Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:
[SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop
Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
Melakukan unzip pada home.zip
Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
documents: .pdf dan .docx. 
images: .jpg, .png, dan .ico. 
website: .js, .html, dan .json. 
sisop: .c dan .sh. 
text: .txt. 
aI: .ipynb dan .csv.

Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu
Password terdapat di file password.bin
Pada folder website
Membuat file csv pada folder website dengan format ini: file,title,body
Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

Tidak dapat menghapus file / folder yang mengandung prefix “restricted”
Pada folder documents
Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut


Membuat file server.c
Pada folder ai, terdapat file webtoon.csv
Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client.
Menampilkan seluruh judul
Menampilkan berdasarkan genre
Menampilkan berdasarkan hari
Menambahkan ke dalam file webtoon.csv
Melakukan delete berdasarkan judul
Selain command yang diberikan akan menampilkan tulisan “Invalid Command”
Manfaatkan client.c pada folder sisop sebagai client

## Kode 

### open-password.c

```
#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include <string.h>

// gcc open-password.c -o open-password -lcrypto
// wget -q -O files.zip https://drive.google.com/u/0/uc?id=1MFQ6ds9lpbo9I6-QlXjyFPYyMEZpB9Tq&export=download

char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}

char *readFile(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("File tidak ditemukan");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *content = (char *)malloc(file_size + 1);
    if (content == NULL) {
        fclose(file);
        perror("Gagal mengalokasi memori");
        exit(1);
    }

    fread(content, 1, file_size, file);
    fclose(file);

    content[file_size] = '\0';

    return content;
}

int main() {
    system("unzip files.zip");
    char *password = readFile("zip-pass.txt");
    printf("Password dari zip-pass.txt: %s\n", password);

    char *decoded_password = decryptBase64(password);
    printf("Password setelah dekripsi base64: %s\n", decoded_password);

    char unzip_command[100];
    sprintf(unzip_command, "unzip -P %s home.zip -d home", decoded_password);
    system(unzip_command);


    free(password);
    free(decoded_password);

    return 0;
}
```

### semangat.c

```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define MAX_PATH_LENGTH 1000
#define MAX_PASSWORD_LENGTH 1024
#define LOG_FILE "logs-fuse.log"

static char password[MAX_PASSWORD_LENGTH];
static const char *dirpath = "/home/josee/sisop-praktikum-modul-4-2023-mh-it03/soal_2/home";

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1) return -errno;

    return 0;
}

void write_log(const char *status, const char *tag, const char *info) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    FILE *log_file = fopen(LOG_FILE, "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
                status, tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, tag, info);
        fclose(log_file);
    }
}

void generateHTML(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");

    if (csvFile == NULL) {
        return;
    }

    char line[512];  // Assuming a maximum line length of 512 characters

    // Read the header line (file,title,body)
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unable to read the header line");
        return; // Unable to read the header line
    }

    // Check if the header line is as expected
    if (strcmp(line, "file,title,body\n") != 0) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unexpected header line");
        return; // Unexpected header line
    }

    // Get the directory of the CSV file
    char csvDir[512];
    strncpy(csvDir, csvPath, strrchr(csvPath, '/') - csvPath + 1);
    csvDir[strrchr(csvPath, '/') - csvPath + 1] = '\0';

    // Continue reading the rest of the CSV file
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char fileName[256], title[256], body[256];

        sscanf(line, "%[^,],%[^,],%[^\n]", fileName, title, body);

        char htmlFileName[512];
        sprintf(htmlFileName, "%s/website/%s.html", dirpath, fileName);

        FILE *htmlFile = fopen(htmlFileName, "w");

        if (htmlFile == NULL) {
            perror("Error creating HTML file");
            write_log("FAILED", "generateHTML", "Error creating HTML file");
            fclose(csvFile);
            return;
        }

        // Write the HTML content to the file
        fprintf(htmlFile, "<!DOCTYPE html>\n");
        fprintf(htmlFile, "<html lang=\"en\">\n");
        fprintf(htmlFile, "<head>\n");
        fprintf(htmlFile, "\t<meta charset=\"UTF-8\" />\n");
        fprintf(htmlFile, "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
        fprintf(htmlFile, "\t<title>%s</title>\n", title);
        fprintf(htmlFile, "</head>\n");
        fprintf(htmlFile, "<body>\n");
        fprintf(htmlFile, "\t%s\n", body);
        fprintf(htmlFile, "</body>\n");
        fprintf(htmlFile, "</html>\n");

        fclose(htmlFile);
        write_log("SUCCESS", "generateHTML", htmlFileName);
    }

    fclose(csvFile);
}

void classifyAndMoveFile(const char *fileName) {
    const char *extensions[] = {".pdf", ".docx", ".jpg", ".png", ".ico", ".js", ".html", ".json", ".c", ".sh", ".txt", ".ipynb", ".csv"};
    const char *folders[] = {"documents", "documents", "images", "images", "images", "website", "website", "website", "sisop", "sisop", "text", "aI", "aI"};
    const char *extension = strrchr(fileName, '.');
    
    if (extension != NULL) {
        for (int i = 0; i < sizeof(extensions) / sizeof(extensions[0]); i++) {
            if (strcmp(extension, extensions[i]) == 0) {
                if (strcmp(extension, ".csv") == 0) {
                    // Assuming CSV files are in aI/
                    char csvPath[512];
                    sprintf(csvPath, "%s/aI/%s", dirpath, basename(fileName));
                    generateHTML(csvPath);

                    // Move the CSV file to the aI/ directory after processing
                    char destPath[512];
                    sprintf(destPath, "%s/aI/%s", dirpath, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                } else {
                    char destFolder[512];
                    sprintf(destFolder, "%s/%s", dirpath, folders[i]);
                    char destPath[512];
                    sprintf(destPath, "%s/%s", destFolder, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                }
                return;
            }
        }
    }
}

void processFilesInDirectory(const char* directoryPath) {
    DIR *dir;
    struct dirent *entry;
    struct stat statBuf;
    char filePath[512];

    dir = opendir(directoryPath);

    if (!dir) {
        perror("Directory tidak dapat dibuka");
        write_log("FAILED", "processFilesInDirectory", "Directory tidak dapat dibuka");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir))) {
        sprintf(filePath, "%s/%s", directoryPath, entry->d_name);

        if (!stat(filePath, &statBuf) && S_ISREG(statBuf.st_mode)) {
            classifyAndMoveFile(filePath);
            write_log("SUCCESS", "processFilesInDirectory", filePath);
        }
    }

    closedir(dir);
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];

    if(strcmp(path,"/") == 0) {
        path=dirpath;
        snprintf(fpath, sizeof(fpath), "%s", path);
    } else snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        write_log("FAILED", "readdir", strerror(errno));
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;

        if (de->d_type == DT_REG) {
            char file_path[MAX_PATH_LENGTH];
            snprintf(file_path, sizeof(file_path), "%s/%s", fpath, de->d_name);
            classifyAndMoveFile(file_path);
            write_log("SUCCESS", "readdir", file_path);
        }
    }

    closedir(dp);
    return 0;
}

static bool is_correct_password(const char *user_password) {
    read_password();
    write_log("SUCCESS", "is_correct_password", "Password read");

    return strcmp(user_password, password) == 0;
}

void read_password() {
    FILE *password_file = fopen("home/password.bin", "rb");

    if (password_file == NULL) {
        perror("Error opening password file");
        write_log("FAILED", "read_password", "Error opening password file");
        exit(EXIT_FAILURE);
    }

    fread(password, sizeof(char), MAX_PASSWORD_LENGTH, password_file);

    fclose(password_file);
    write_log("SUCCESS", "read_password", "Password read");
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        write_log("FAILED", "read", strerror(errno));
        return -errno;
    }

    if (strstr(fpath, "text/") != NULL) {
        bool correct_password = false;

        while (!correct_password) {
            char user_password[MAX_PASSWORD_LENGTH];
            printf("Enter password ");
            scanf("%s", user_password);

            correct_password = is_correct_password(user_password);

            if (!correct_password) {
                printf("Incorrect password.\n");
            }
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        write_log("FAILED", "read", strerror(errno));
        res = -errno;
    }

    close(fd);

    write_log("SUCCESS", "read", fpath);

    return res;
}

static int xmp_setfattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);
    if (res == -1) {
        write_log("FAILED", "setfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "setfattr", fpath);

    return 0;
}

static int xmp_getfattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lgetxattr(fpath, name, value, size);
    if (res == -1) {
        write_log("FAILED", "getfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "getfattr", fpath);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags);
    if (res == -1) {
        write_log("FAILED", "open", strerror(errno));
        return -errno;
    }

    fi->fh = res;

    write_log("SUCCESS", "open", fpath);

    return 0;
}

static int xmp_unlink(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the file or folder has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "unlink", "Deleting files/folders with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = unlink(fpath);
    if (res == -1) {
        write_log("FAILED", "unlink", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "unlink", fpath);

    return 0;
}


static int xmp_rmdir(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the directory has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "rmdir", "Deleting directories with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = rmdir(fpath);
    if (res == -1) {
        write_log("FAILED", "rmdir", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "rmdir", fpath);

    return 0;
}


static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int fd = open(fpath, fi->flags | O_WRONLY);
    if (fd == -1) {
        write_log("FAILED", "write", strerror(errno));
        return -errno;
    }

    // Move the file pointer to the specified offset
    if (lseek(fd, offset, SEEK_SET) == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    // Write data to the file
    ssize_t res = write(fd, buf, size);
    if (res == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    close(fd);

    // Log the successful write operation
    write_log("SUCCESS", "write", fpath);

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .setxattr = xmp_setfattr,
    .getxattr = xmp_getfattr,
    .open = xmp_open,
    .unlink = xmp_unlink, 
    .rmdir = xmp_rmdir, 
    .write = xmp_write,
};


int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(LOG_FILE, "w");
    if (log_file != NULL) {
        fclose(log_file);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

//  gcc -Wall -D_FILE_OFFSET_BITS=64 `pkg-config fuse --cflags` semangat.c -o semangat `pkg-config fuse --libs` -lfuse

// setfattr -n user.name -v "bangudahbang" Belajar-Python.pdf
// getfattr -n user.name Belajar-Python.pdf
```

### server.c

```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

// Fungsi untuk memproses pesan dari client
void process_message(int new_socket, char *message) {
    FILE *file;
    char buffer[4096] = {0};
    char reply_message[4096] = {0};

    if (strncmp(message, "showAll", 7) == 0) {
        // Open the file webtoon.csv
        file = fopen("home/aI/webtoon.csv", "r");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Send webtoons to the client in chunks
        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, sizeof(buffer) - 1, file)) > 0) {
            buffer[bytesRead] = '\0';  // Null-terminate the buffer
            send(new_socket, buffer, bytesRead, 0);
            memset(buffer, 0, sizeof(buffer));
        }

        fclose(file);
} else if (strncmp(message, "showGenre:", 10) == 0) {
    // Extract the genre from the message
    char *genre = strtok(message + 11, " ");
    genre[strlen(genre) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified genre to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified genre
        if (strstr(buffer, genre) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "showDate:", 9) == 0) {
    // Extract the date from the message
    char *date = strtok(message + 10, " ");
    date[strlen(date) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified date to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified date
        if (strstr(buffer, date) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "add:", 4) == 0) {
        // Extract the new webtoon information from the message
        char *newWebtoon = strtok(message + 4, "\n");

        file = fopen("home/aI/webtoon.csv", "a");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Append the new webtoon to the file
        fprintf(file, "%s\n", newWebtoon);

        fclose(file);

        // Send a confirmation message to the client
        strcpy(reply_message, "Webtoon added successfully");
        send(new_socket, reply_message, strlen(reply_message), 0);

} else if (strncmp(message, "delete:", 7) == 0) {
    // Extract the content to be deleted from the message
    char* contentToDelete = message + 7;

    // Open the file webtoon.csv in read mode
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Open a temporary file in write mode
    FILE* tempFile = fopen("home/aI/temp_webtoon.csv", "w");
    if (!tempFile) {
        perror("Temporary file error");
        strcpy(reply_message, "Error creating temporary file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        fclose(file);
        return;
    }

    char line[1024];
    int contentDeleted = 0;

    // Read the file line by line
    while (fgets(line, sizeof(line), file)) {
        // Check if the line contains the content to be deleted
        if (strstr(line, contentToDelete) == NULL) {
            // If not, write the line to the temporary file
            fputs(line, tempFile);
        } else {
            // If yes, set a flag to indicate content deletion
            contentDeleted = 1;
        }
    }

    // Close both files
    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove("home/aI/webtoon.csv");
    rename("home/aI/temp_webtoon.csv", "home/aI/webtoon.csv");

    if (contentDeleted) {
        strcpy(reply_message, "Content deleted successfully");
    } else {
        strcpy(reply_message, "Content not found for deletion");
    }

    send(new_socket, reply_message, strlen(reply_message), 0);
} else {
        strcpy(reply_message, "Invalid request");
        send(new_socket, reply_message, strlen(reply_message), 0);
    }
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[4096] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket option to allow address reuse
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

while (1) {
    valread = read(new_socket, buffer, 1024);
    if (valread <= 0) {
        break;
    }

    // Remove the newline character if present
    size_t len = strlen(buffer);
    if (len > 0 && buffer[len - 1] == '\n') {
        buffer[len - 1] = '\0';
    }

    printf("Client: %s\n", buffer);

    process_message(new_socket, buffer);

    memset(buffer, 0, sizeof(buffer));
}

    close(new_socket);

    return 0;
}
```

## Analisis Code 

### open-password.c

```
char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}
```

fungsi untuk melakukan decrypt base64.

```
char *readFile(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("File tidak ditemukan");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *content = (char *)malloc(file_size + 1);
    if (content == NULL) {
        fclose(file);
        perror("Gagal mengalokasi memori");
        exit(1);
    }

    fread(content, 1, file_size, file);
    fclose(file);

    content[file_size] = '\0';

    return content;
}
```

fungsi untuk membuka isi dari file yang diinginkan, dan return isi dari file tersebut dalam bentuk string.

```
int main() {
    system("unzip files.zip");
    char *password = readFile("zip-pass.txt");
    printf("Password dari zip-pass.txt: %s\n", password);

    char *decoded_password = decryptBase64(password);
    printf("Password setelah dekripsi base64: %s\n", decoded_password);

    char unzip_command[100];
    sprintf(unzip_command, "unzip -P %s home.zip -d home", decoded_password);
    system(unzip_command);


    free(password);
    free(decoded_password);

    return 0;
}
```

fungsi main yang berisi pemanggilan fungsi decodeBase64 dan openFile yang kemudian outputnya disimpan pada variabel password. Dan fungsi unzip pada file yang sudah didownload tadi. Kemudian terakhir pemanggilan fungsi untuk unzip dengan menyertakan password yang sudah didecrypt tadi. Terakhir, dilakukan pembebasan memory dari variabel password dan decoded_password.

### semangat.c

```
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1) return -errno;

    return 0;
}
```

fungsi untuk memberikan akses pada fuse untuk membuat directory pada folder yang dimounting.

```
static int xmp_setfattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);
    if (res == -1) {
        write_log("FAILED", "setfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "setfattr", fpath);

    return 0;
}

```

fungsi untuk memberikan atribut tambahan pada file.

```
static int xmp_getfattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lgetxattr(fpath, name, value, size);
    if (res == -1) {
        write_log("FAILED", "getfattr", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "getfattr", fpath);

    return res;
}
```

fungsi untuk mendapatkan atribut yang sudah ditambahkan user pada file.

```
static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags);
    if (res == -1) {
        write_log("FAILED", "open", strerror(errno));
        return -errno;
    }

    fi->fh = res;

    write_log("SUCCESS", "open", fpath);

    return 0;
}
```

fungsi untuk memberikan akses membuka file pada folder yang dimounting.

```
static int xmp_unlink(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the file or folder has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "unlink", "Deleting files/folders with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = unlink(fpath);
    if (res == -1) {
        write_log("FAILED", "unlink", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "unlink", fpath);

    return 0;
}
```

fungsi untuk memberikan akses menghapus file pada folder yang dimounting.

```
static int xmp_rmdir(const char *path) {
    char fpath[MAX_PATH_LENGTH];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    // Check if the directory has the prefix "restricted"
    if (strstr(basename(fpath), "restricted") != NULL) {
        write_log("FAILED", "rmdir", "Deleting directories with 'restricted' prefix is not allowed");
        return -EPERM;  // Return a permission error
    }

    int res = rmdir(fpath);
    if (res == -1) {
        write_log("FAILED", "rmdir", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "rmdir", fpath);

    return 0;
}
```

fungsi untuk memberikan akses menghapus folder pada directory yang dimounting.

```
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int fd = open(fpath, fi->flags | O_WRONLY);
    if (fd == -1) {
        write_log("FAILED", "write", strerror(errno));
        return -errno;
    }

    // Move the file pointer to the specified offset
    if (lseek(fd, offset, SEEK_SET) == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    // Write data to the file
    ssize_t res = write(fd, buf, size);
    if (res == -1) {
        write_log("FAILED", "write", strerror(errno));
        close(fd);
        return -errno;
    }

    close(fd);

    // Log the successful write operation
    write_log("SUCCESS", "write", fpath);

    return res;
}
```

fungsi untuk memberikan akses mengubah isi dari file di directory yang dimounting.

```
void write_log(const char *status, const char *tag, const char *info) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    FILE *log_file = fopen(LOG_FILE, "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
                status, tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, tag, info);
        fclose(log_file);
    }
}
```

fungsi untuk membuat log dengan format yang sesuai dengan soal. Fungsi ini akan dipanggil di tiap fungsi untuk menandakan apakah fungsi tersebut berhasil dijalankan atau tidak.

```
void generateHTML(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");

    if (csvFile == NULL) {
        return;
    }

    char line[512];  // Assuming a maximum line length of 512 characters

    // Read the header line (file,title,body)
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unable to read the header line");
        return; // Unable to read the header line
    }

    // Check if the header line is as expected
    if (strcmp(line, "file,title,body\n") != 0) {
        fclose(csvFile);
        write_log("FAILED", "generateHTML", "Unexpected header line");
        return; // Unexpected header line
    }

    // Get the directory of the CSV file
    char csvDir[512];
    strncpy(csvDir, csvPath, strrchr(csvPath, '/') - csvPath + 1);
    csvDir[strrchr(csvPath, '/') - csvPath + 1] = '\0';

    // Continue reading the rest of the CSV file
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char fileName[256], title[256], body[256];

        sscanf(line, "%[^,],%[^,],%[^\n]", fileName, title, body);

        char htmlFileName[512];
        sprintf(htmlFileName, "%s/website/%s.html", dirpath, fileName);

        FILE *htmlFile = fopen(htmlFileName, "w");

        if (htmlFile == NULL) {
            perror("Error creating HTML file");
            write_log("FAILED", "generateHTML", "Error creating HTML file");
            fclose(csvFile);
            return;
        }

        // Write the HTML content to the file
        fprintf(htmlFile, "<!DOCTYPE html>\n");
        fprintf(htmlFile, "<html lang=\"en\">\n");
        fprintf(htmlFile, "<head>\n");
        fprintf(htmlFile, "\t<meta charset=\"UTF-8\" />\n");
        fprintf(htmlFile, "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
        fprintf(htmlFile, "\t<title>%s</title>\n", title);
        fprintf(htmlFile, "</head>\n");
        fprintf(htmlFile, "<body>\n");
        fprintf(htmlFile, "\t%s\n", body);
        fprintf(htmlFile, "</body>\n");
        fprintf(htmlFile, "</html>\n");

        fclose(htmlFile);
        write_log("SUCCESS", "generateHTML", htmlFileName);
    }

    fclose(csvFile);
}
```

fungsi untuk membuat file .html di directory website berdasarkan isi file .csv yang ada di directory aI dengan format file .csv file,title,body. Isi dari file HTML akan sesuai dengan format dari .csv, di mana nama akan sesuai dengan file, title sesuai dengan title, dan body sesuai dengan body.

```
void classifyAndMoveFile(const char *fileName) {
    const char *extensions[] = {".pdf", ".docx", ".jpg", ".png", ".ico", ".js", ".html", ".json", ".c", ".sh", ".txt", ".ipynb", ".csv"};
    const char *folders[] = {"documents", "documents", "images", "images", "images", "website", "website", "website", "sisop", "sisop", "text", "aI", "aI"};
    const char *extension = strrchr(fileName, '.');
    
    if (extension != NULL) {
        for (int i = 0; i < sizeof(extensions) / sizeof(extensions[0]); i++) {
            if (strcmp(extension, extensions[i]) == 0) {
                if (strcmp(extension, ".csv") == 0) {
                    // Assuming CSV files are in aI/
                    char csvPath[512];
                    sprintf(csvPath, "%s/aI/%s", dirpath, basename(fileName));
                    generateHTML(csvPath);

                    // Move the CSV file to the aI/ directory after processing
                    char destPath[512];
                    sprintf(destPath, "%s/aI/%s", dirpath, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                } else {
                    char destFolder[512];
                    sprintf(destFolder, "%s/%s", dirpath, folders[i]);
                    char destPath[512];
                    sprintf(destPath, "%s/%s", destFolder, basename(fileName));
                    rename(fileName, destPath);
                    write_log("SUCCESS", "classifyAndMoveFile", destPath);
                }
                return;
            }
        }
    }
}
```

Fungsi untuk melakukan classfiy file pada directory mounting. klasifikasi dilakukan dengan mencocokkan ekstensi dari file, dan akan dimasukkan ke directory yang sesuai. Di fungsi ini juga dilakukan pemanggilan fungsi generateHTML untuk melakukan scanning pada file .csv di directory aI.

```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];

    if(strcmp(path,"/") == 0) {
        path=dirpath;
        snprintf(fpath, sizeof(fpath), "%s", path);
    } else snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        write_log("FAILED", "readdir", strerror(errno));
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;

        if (de->d_type == DT_REG) {
            char file_path[MAX_PATH_LENGTH];
            snprintf(file_path, sizeof(file_path), "%s/%s", fpath, de->d_name);
            classifyAndMoveFile(file_path);
            write_log("SUCCESS", "readdir", file_path);
        }
    }

    closedir(dp);
    return 0;
}
```

fungsi ini untuk memberikan akses pada folder mounting untuk melakukan pembacaan directory seperti ls. Pada fungsi ini juga ada pemanggilan fungsi classfiyFiles. ClassifyFiles menggunakan fungsi readdir sebagai trigger untuk melakukan scanning. Jadi tiap kali kita melakukan ls pada folder mounting classfiy akan berjalan.

```
static bool is_correct_password(const char *user_password) {
    read_password();
    write_log("SUCCESS", "is_correct_password", "Password read");

    return strcmp(user_password, password) == 0;
}

void read_password() {
    FILE *password_file = fopen("home/password.bin", "rb");

    if (password_file == NULL) {
        perror("Error opening password file");
        write_log("FAILED", "read_password", "Error opening password file");
        exit(EXIT_FAILURE);
    }

    fread(password, sizeof(char), MAX_PASSWORD_LENGTH, password_file);

    fclose(password_file);
    write_log("SUCCESS", "read_password", "Password read");
}
```

kedua fungsi di atas berguna untuk melakukan scanning pada isi file password.bin.

```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[MAX_PATH_LENGTH];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        write_log("FAILED", "read", strerror(errno));
        return -errno;
    }

    if (strstr(fpath, "text/") != NULL) {
        bool correct_password = false;

        while (!correct_password) {
            char user_password[MAX_PASSWORD_LENGTH];
            printf("Enter password ");
            scanf("%s", user_password);

            correct_password = is_correct_password(user_password);

            if (!correct_password) {
                printf("Incorrect password.\n");
            }
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        write_log("FAILED", "read", strerror(errno));
        res = -errno;
    }

    close(fd);

    write_log("SUCCESS", "read", fpath);

    return res;
}
```

Dilakukan pemanggilan fungsi correct password pada fungsi read agar tiap kali user ingin melakukan cat pada directory text, user harus melakukan input password yang sesuai dengan isi file password.bin.

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .setxattr = xmp_setfattr,
    .getxattr = xmp_getfattr,
    .open = xmp_open,
    .unlink = xmp_unlink, 
    .rmdir = xmp_rmdir, 
    .write = xmp_write,
};


int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(LOG_FILE, "w");
    if (log_file != NULL) {
        fclose(log_file);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

Berikut adalah struct yang berisi fungsi-fungsi akses dari program fuse untuk folder yang dimounting. Dan dibawahnya adalah fungsi main dari program di atas, untuk memanggil fungsi-fungsi fuse tadi.

### server.c

```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

// Fungsi untuk memproses pesan dari client
void process_message(int new_socket, char *message) {
    FILE *file;
    char buffer[4096] = {0};
    char reply_message[4096] = {0};

    if (strncmp(message, "showAll", 7) == 0) {
        // Open the file webtoon.csv
        file = fopen("home/aI/webtoon.csv", "r");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Send webtoons to the client in chunks
        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, sizeof(buffer) - 1, file)) > 0) {
            buffer[bytesRead] = '\0';  // Null-terminate the buffer
            send(new_socket, buffer, bytesRead, 0);
            memset(buffer, 0, sizeof(buffer));
        }

        fclose(file);
} else if (strncmp(message, "showGenre:", 10) == 0) {
    // Extract the genre from the message
    char *genre = strtok(message + 11, " ");
    genre[strlen(genre) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified genre to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified genre
        if (strstr(buffer, genre) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "showDate:", 9) == 0) {
    // Extract the date from the message
    char *date = strtok(message + 10, " ");
    date[strlen(date) - 1] = '\0';  // Remove the trailing newline character

    // Open the file webtoon.csv
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Send webtoons of the specified date to the client
    while (fgets(buffer, sizeof(buffer), file)) {
        // Check if the line contains the specified date
        if (strstr(buffer, date) != NULL) {
            send(new_socket, buffer, strlen(buffer), 0);
            memset(buffer, 0, sizeof(buffer));
        }
    }

    fclose(file);

    } else if (strncmp(message, "add:", 4) == 0) {
        // Extract the new webtoon information from the message
        char *newWebtoon = strtok(message + 4, "\n");

        file = fopen("home/aI/webtoon.csv", "a");
        if (!file) {
            perror("File error");
            strcpy(reply_message, "Error opening file");
            send(new_socket, reply_message, strlen(reply_message), 0);
            return;
        }

        // Append the new webtoon to the file
        fprintf(file, "%s\n", newWebtoon);

        fclose(file);

        // Send a confirmation message to the client
        strcpy(reply_message, "Webtoon added successfully");
        send(new_socket, reply_message, strlen(reply_message), 0);

} else if (strncmp(message, "delete:", 7) == 0) {
    // Extract the content to be deleted from the message
    char* contentToDelete = message + 7;

    // Open the file webtoon.csv in read mode
    file = fopen("home/aI/webtoon.csv", "r");
    if (!file) {
        perror("File error");
        strcpy(reply_message, "Error opening file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        return;
    }

    // Open a temporary file in write mode
    FILE* tempFile = fopen("home/aI/temp_webtoon.csv", "w");
    if (!tempFile) {
        perror("Temporary file error");
        strcpy(reply_message, "Error creating temporary file");
        send(new_socket, reply_message, strlen(reply_message), 0);
        fclose(file);
        return;
    }

    char line[1024];
    int contentDeleted = 0;

    // Read the file line by line
    while (fgets(line, sizeof(line), file)) {
        // Check if the line contains the content to be deleted
        if (strstr(line, contentToDelete) == NULL) {
            // If not, write the line to the temporary file
            fputs(line, tempFile);
        } else {
            // If yes, set a flag to indicate content deletion
            contentDeleted = 1;
        }
    }

    // Close both files
    fclose(file);
    fclose(tempFile);

    // Replace the original file with the temporary file
    remove("home/aI/webtoon.csv");
    rename("home/aI/temp_webtoon.csv", "home/aI/webtoon.csv");

    if (contentDeleted) {
        strcpy(reply_message, "Content deleted successfully");
    } else {
        strcpy(reply_message, "Content not found for deletion");
    }

    send(new_socket, reply_message, strlen(reply_message), 0);
} else {
        strcpy(reply_message, "Invalid request");
        send(new_socket, reply_message, strlen(reply_message), 0);
    }
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[4096] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket option to allow address reuse
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

while (1) {
    valread = read(new_socket, buffer, 1024);
    if (valread <= 0) {
        break;
    }

    // Remove the newline character if present
    size_t len = strlen(buffer);
    if (len > 0 && buffer[len - 1] == '\n') {
        buffer[len - 1] = '\0';
    }

    printf("Client: %s\n", buffer);

    process_message(new_socket, buffer);

    memset(buffer, 0, sizeof(buffer));
}

    close(new_socket);

    return 0;
}

```

Header dan Pendefinisian Konstanta:

Memasukkan beberapa header yang diperlukan untuk operasi soket dan fungsi-fungsi standar.
Mendefinisikan PORT (nomor port) yang digunakan oleh server (8080).
Fungsi process_message:

Fungsi ini dipanggil untuk memproses pesan yang diterima dari klien.
Memiliki logika yang berbeda tergantung pada pesan yang diterima:
Pesan "showAll" membuka file webtoon.csv dan mengirimkan isinya ke klien.
Pesan "showGenre:" membaca genre dari pesan, mencari webtoon dengan genre tersebut, dan mengirimnya ke klien.
Pesan "showDate:" membaca tanggal dari pesan, mencari webtoon dengan tanggal tersebut, dan mengirimnya ke klien.
Pesan "add:" menambahkan webtoon baru ke dalam file webtoon.csv.
Pesan "delete:" menghapus konten yang sesuai dari file webtoon.csv.
Fungsi main:

main inisialisasi soket server dan menunggu koneksi dari klien.
Setelah koneksi terbentuk, server membaca pesan dari klien secara terus-menerus dalam loop.
Setiap pesan yang diterima akan diproses menggunakan fungsi process_message.

## Output

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/be2ecc8a-6bf4-4bc8-9ff7-f5d88c5b01ce)

hasil compile open-password.c

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/d3924384-aa87-4261-b9b8-831c6d468a5e)

keadaan directory sebelum klasifikasi

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/131e1a90-4794-44ed-8955-4f8f427f7a78)

keaddan directory setelah klasifikasi

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/52fd7010-fec6-4645-9e50-508da3b2af14)

./semangat akan meminta password ketika dilakukan cat pada directory text.

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/7b4b1739-9742-4145-aa48-a7515fa64ec7)

file html yang di generate sesuai dengan isi file .csv.

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/525546a4-a946-4813-8c4c-44e5dfbbe1d5)

file dengan prefix restricted tidak dapat dihapus.

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/6a816658-0bf4-4159-a920-0c7ec1943738)

men set attribute dan mendapatkan attribute tadi pada file di folder documents.

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/3cba174a-d9f4-45a5-af1f-aca240345da6)

menampilkan seluruh isi file webtoon.csv

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/60642964-7729-4f71-a97a-5e6a86245686)

menampilkan berdasarkan genre

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/63e28be7-8f2d-4891-bbc3-442c49171447)

menampilkan berdasarkan hari

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/723cebfb-f272-4807-bb32-4eb0ed2493ef)

menambahkan isi dari webtoon.csv

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/f31a19be-d101-4663-97b1-1451c2238e09)

menghapus line di webtoon.csv

![image](https://github.com/J0see1/pweb-vue-P19-2023/assets/134209563/2ff7273c-27a7-48bc-a452-b86d9903ea1c)

ketika command yang diberikan client tidak sesuai pada fungsi di server.c

# Soal 3

easy.c

Berikut dibawah ini program dari easy.c , yang dimana fungsi dari program ini sendiri yaitu untuk membuat suatu file system, yang dimana isi dari file system itu sendiri akan dipecah menjadi 3 file dengan masing masing beban memory sebanyak 1024 bytes apabila nama dari file itu diubah dengan awalan 'module_', dan juga program ini nantinya akan membuat suatu file log sebagai report dari program yang dijalankan. di bawah ini merupakan script easy.c

    #define FUSE_USE_VERSION 30
    #include <fuse.h>
    #include <stdio.h>
    #include <string.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <dirent.h>
    #include <errno.h>
    #include <time.h>

    static const char *fs_path = "/tmp/modular"; //menentukan path dari file system yang akan diatur
    static const char *log_path = "/home/dave/fs_module.log"; //menentukan path untuk file log

    //fungsi untuk mencatat sistem call ke dalam file log
    void log_system_call(char *level, char *command, char *description) {
    time_t t; //variabel untuk menyimpan satuan waktu
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20]; //membuat format waktu 
    strftime(timestamp, 20, "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a"); //membuka file log untuk menambahkan data
    if (log_file) {
        fprintf(log_file, "[%s]::%s::%s::%s\n", level, timestamp, command, description);
        fclose(log_file);
    }
    }

    //fungsi untuk memodularisasi file dengan ukuran 1024 bytes
    //membaca file dan membuatnya menjadi bagian - bagian kecil
    void modularize_file(char *file_path) {
    FILE *original_file = fopen(file_path, "rb");
    if (original_file) {
        fseek(original_file, 0L, SEEK_END);
        long file_size = ftell(original_file);
        rewind(original_file);

        if (file_size > 1024) {
            char buffer[1024];
            int index = 0;

            while (1) {
                size_t read_size = fread(buffer, 1, sizeof(buffer), original_file);

                if (read_size > 0) {
                    char new_file_path[256];
                    snprintf(new_file_path, sizeof(new_file_path), "%s.%03d", file_path, index);
                    FILE *new_file = fopen(new_file_path, "wb");

                    if (new_file) {
                        fwrite(buffer, 1, read_size, new_file);
                        fclose(new_file);
                    }

                    index++;
                } else {
                    break;
                }
            }

            fclose(original_file);
            remove(file_path);
        }
    }
    }

    //fungsi untuk memodularisasi seluruh isi dari directory bahkan subdirectory
    //melakukan rekursi ke dalam sub directory
    void modularize_directory(const char *directory) {
    DIR *dir = opendir(directory);
    struct dirent *entry;

    if (dir) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) {
                char file_path[256];
                snprintf(file_path, sizeof(file_path), "%s/%s", directory, entry->d_name);
                modularize_file(file_path);
            } else if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                char subdirectory[256];
                snprintf(subdirectory, sizeof(subdirectory), "%s/%s", directory, entry->d_name);
                modularize_directory(subdirectory);
            }
        }

        closedir(dir);
    }
    }

    static int fs_getattr(const char *path, struct stat *stbuf) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return res;
    }

    static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dir;
    struct dirent *entry;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    dir = opendir(fpath);
    if (dir == NULL)
        return -errno;

    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0))
            break;
    }

    closedir(dir);
    return 0;
    }

    static int fs_mkdir(const char *path, mode_t mode) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;

    if (strstr(path, "module_") != NULL) {
        log_system_call("REPORT", "CREATE", fpath);
        modularize_directory(fpath);
    }

    return res;
    }

    static int fs_rename(const char *oldpath, const char *newpath) {
    int res = 0;
    char old_fpath[256];
    char new_fpath[256];

    snprintf(old_fpath, sizeof(old_fpath), "%s%s", fs_path, oldpath);
    snprintf(new_fpath, sizeof(new_fpath), "%s%s", fs_path, newpath);

    res = rename(old_fpath, new_fpath);
    if (res == -1)
        return -errno;

    if ((strstr(oldpath, "module_") != NULL) || (strstr(newpath, "module_") != NULL)) {
        log_system_call("REPORT", "RENAME", strcat(strcat(old_fpath, "::"), new_fpath));
        modularize_directory(new_fpath);
    }

    return res;
    }

    static int fs_rmdir(const char *path) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    log_system_call("FLAG", "RMDIR", fpath);

    return res;
    }

    static int fs_unlink(const char *path) {
    int res = 0;
    char fpath[256];

    snprintf(fpath, sizeof(fpath), "%s%s", fs_path, path);

    res = unlink(fpath);
    if (res == -1)
        return -errno;

    log_system_call("FLAG", "UNLINK", fpath);

    return res;
    }

    //fungsi dasar dari fuse
    static struct fuse_operations fs_operations = {
    .getattr = fs_getattr, //mengambil atribut dari suatu file atau directory
    .readdir = fs_readdir, //mambaca isi dari sebuah directory
    .mkdir = fs_mkdir, //membuat directory baru
    .rename = fs_rename, //mengganti nama sebuah directory atau file
    .rmdir = fs_rmdir, //hapus directory
    .unlink = fs_unlink, //hapus file
    };

    int main(int argc, char *argv[]) {
    return fuse_main(argc, argv, &fs_operations, NULL); //memanggil fungsi fuse
    }

Penjelasan Tiap fungsi

    #define FUSE_USE_VERSION 30
    #include <fuse.h>
    #include <stdio.h>
    #include <string.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <dirent.h>
    #include <errno.h>
    #include <time.h>

#define FUSE_USE_VERSION 30: Mendefinisikan versi FUSE yang digunakan oleh program.

#include <fuse.h>: Mengimpor library FUSE untuk membuat filesystem dalam userspace.

<stdio.h>, <string.h>, <stdlib.h>, <unistd.h>, <fcntl.h>, <sys/types.h>, <sys/stat.h>, <dirent.h>, <errno.h>, <time.h>: Mengimpor library C standar yang dibutuhkan untuk operasi input/output, manipulasi string, alokasi memori, manipulasi file, dan lainnya.

    static const char *fs_path = "/home/index/modular";
    static const char *log_path = "/home/user/fs_module.log";

static const char *fs_path = "/home/index/modular";: Menentukan path dari filesystem yang akan diatur. Dalam hal ini, path tersebut adalah "/home/index/modular".

static const char *log_path = "/home/user/fs_module.log";: Menentukan path untuk file log, yaitu "/home/user/fs_module.log".

    void log_system_call(char *level, char *command, char *description) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, 20, "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a");
    if (log_file) {
        fprintf(log_file, "[%s]::%s::%s::%s\n", level, timestamp, command, description);
        fclose(log_file);
        }
    }

void log_system_call(char *level, char *command, char *description): Fungsi untuk mencatat system call ke dalam file log.

time_t t; struct tm *tm_info;: Variabel untuk menyimpan waktu dan informasi waktu.

time(&t); tm_info = localtime(&t);: Mendapatkan waktu saat ini dan menyimpannya dalam variabel tm_info.

char timestamp[20]; strftime(timestamp, 20, "%y%m%d-%H:%M:%S", tm_info);: Membuat string timestamp dengan format "yymmdd-HH:MM:SS".

FILE *log_file = fopen(log_path, "a");: Membuka file log untuk ditambahkan data (mode "a" untuk append).

fprintf(log_file, "[%s]::%s::%s::%s\n", level, timestamp, command, description);: Menulis log ke dalam file dengan format tertentu.

fclose(log_file);: Menutup file log setelah penulisan selesai.

    void modularize_file(char *file_path) {
    // ... (implementasi fungsi modularize_file)
    }

void modularize_file(char *file_path): Fungsi untuk memodularisasi file dengan membagi menjadi beberapa bagian kecil jika ukurannya lebih besar dari 1024 bytes.

Implementasi fungsi ini akan membaca file, membaginya menjadi bagian-bagian kecil, dan menyimpannya sebagai file-file kecil.

    void modularize_directory(const char *directory) {
    // ... (implementasi fungsi modularize_directory)
    }

void modularize_directory(const char *directory): Fungsi untuk memodularisasi seluruh isi dari sebuah direktori, termasuk sub-direktori.

Implementasi fungsi ini akan memanggil modularize_file untuk setiap file di dalam direktori, dan melakukan rekursi ke dalam sub-direktori.

    static int fs_getattr(const char *path, struct stat *stbuf) {
    // ... (implementasi fungsi fs_getattr)
    }

    static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    // ... (implementasi fungsi fs_readdir)
    }

    static int fs_mkdir(const char *path, mode_t mode) {
    // ... (implementasi fungsi fs_mkdir)
    }

    static int fs_rename(const char *oldpath, const char *newpath) {
    // ... (implementasi fungsi fs_rename)
    }

    static int fs_rmdir(const char *path) {
    // ... (implementasi fungsi fs_rmdir)
    }

    static int fs_unlink(const char *path) {
    // ... (implementasi fungsi fs_unlink)
    }

Fungsi-fungsi ini merupakan implementasi operasi-operasi dasar yang dapat dilakukan pada filesystem menggunakan FUSE.

fs_getattr: Mendapatkan atribut dari file atau direktori.

fs_readdir: Membaca isi dari sebuah direktori.

fs_mkdir: Membuat direktori baru.

fs_rename: Mengganti nama sebuah file atau direktori.

fs_rmdir: Menghapus sebuah direktori.

fs_unlink: Menghapus sebuah file.

    static struct fuse_operations fs_operations = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .mkdir = fs_mkdir,
    .rename = fs_rename,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
    };

static struct fuse_operations fs_operations = { ... };: Mendefinisikan operasi-operasi yang akan digunakan oleh FUSE.

.getattr, .readdir, .mkdir, .rename, .rmdir, .unlink: Menentukan fungsi-fungsi yang akan dipanggil untuk masing-masing operasi tersebut.

    int main(int argc, char *argv[]) {
    return fuse_main(argc, argv, &fs_operations, NULL);
    }

int main(int argc, char *argv[]): Fungsi utama program.

fuse_main(argc, argv, &fs_operations, NULL): Memulai FUSE dengan menggunakan operasi yang telah didefinisikan sebelumnya.
